import glob
import os
import shutil
import subprocess
import sys
import logging
import datetime
import configparser
import psycopg2 as pgdb


# create logger for logging purposes
lgr = logging.getLogger('resultfile')
lgr.setLevel(logging.DEBUG)
# add a file handler
fh = logging.FileHandler('resultfile.log')
fh.setLevel(logging.DEBUG)
# create a formatter and set the formatter for the handler.
frmt = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh.setFormatter(frmt)
# add the Handler to the logger
lgr.addHandler(fh)

run_time = {}  # to save run time process for all lab files
# testfolder_path = os.getcwd() + '\\testFolder\\'
pm_exe = 'ProsodyMain.exe'  # Location of ProsodyMaker

print ('Fetching voices from Database...')
connection = pgdb.connect("dbname='voicebank' user='vbuser' host='localhost' port='5433' password='gfdsa12345!!'")
cursor = connection.cursor()
cursor.execute('select name,smi_voice_id from smi_voice WHERE supported is TRUE ORDER BY smi_voice_id')
columns = cursor.description

voices_list = []
index_r = 1
print ('Available Voices:')
for value in cursor.fetchall():
    tmp = {}
    tmp[value[0]] = value[1]
    print ('{0}   {1}'.format(index_r, value[0]))
    voices_list.append(tmp)
    index_r += 1

# Close connections
cursor.close()
connection.close()

index_r -= 1
voice = input('Please choose a voice index from above: (between 1 and {0})  '.format(index_r))
# print (voice)
voice = int(voice) - 1
voiceID = 0
key = ''
for k, v in voices_list[voice].items():
    print('\nChosen voice: {0}   {1}\n'.format(k, v))
    key = k
    voiceID = v

voice = voice + 1
# print ("voiceID {0}".format(voiceID))

# Get voice index from the user
if voice < 1 and voice > index_r:
    lgr.warn('Voice is not valid')
    print('Voice is not valid')
    sys.exit()

# Get test type from the user
input_folder = input('Please choose a test type: (1 or 2):\n'
                     '1 smoketest\n'
                     '2 functionaltest\n')

input_folder = int(input_folder)
if input_folder == 1:
    input_folder = 'smoketest'
elif input_folder == 2:
    input_folder = 'functionaltest'
else:
    lgr.warn('Input folder is not valid')
    print('Input folder is not valid')
    sys.exit()

voice = str(voice)

# pyshare_folder = 'G:/pyshare/'+str(voiceID)+'/'
# pyshare_dir = ''
tomcat_folder = 'E:/Apache Software Foundation/Tomcat 8.5/'

finished_wav_files = []
textIDs = []

# import info from conf.txt
config = configparser.ConfigParser()
config.read('conf.txt')

synthesize_version = ''
type_t = ''
training_version = ''
training_sentences = ''

section = key
print ("voice: {0}, input_folder: {1}, section: {2}".format(voice, input_folder, section))
lgr.info("voice: {0}, input_folder: {1}, section: {2}".format(voice, input_folder, section))

try:
    training_version = config[section]['Training Version']
    training_sentences = config[section]['Training Sentences']
    synthesize_version = config[section]['Synthesize Version']
    type_t = config[section]['Type']
    voice_name = config[section]['Voice Name']
except:
    # if there is no sesctin then take the default
    training_version = config['Default']['Training Version']
    training_sentences = config['Default']['Training Sentences']
    synthesize_version = config['Default']['Synthesize Version']
    type_t = config['Default']['Type']
    voice_name = config['Default']['Voice Name']

# file_suffix = '.' + ''.join([i for i in synthesize_version if i.isdigit()]) + 'S' + type_t + '.nvc_'\
#               + ''.join([i for i in training_version if i.isdigit()]) + type_t + '_' + training_sentences
# folder_name = 'V' + filter(str.isdigit, training_version) + '_' + type_t + '_' + training_sentences + '_'\
#               + 'V' + filter(str.isdigit, synthesize_version)


# choose the right input folder to process the files within
if input_folder == 'smoketest':
    testfolder_path = os.getcwd() + '\\testFolder\\smoketest\\'
elif input_folder == 'functionaltest':
    testfolder_path = os.getcwd() + '\\testFolder\\functionaltest\\'


#########################################################
# Get the name of all the txt files from the testFolder
#########################################################
lgr.info('Getting txt files names from testFolder')
files_names = glob.glob(testfolder_path + '\*.txt')
lgr.info('Files found: {0}'.format(files_names))
# if there are no files in TestFolder then exit
if files_names == "":
    lgr.warn('TestFolder is empty')
    sys.exit()

for file_name in files_names:
    lgr.info('##########################################\n'
             'Processing file: {0}'.format(file_name))
    start_time = datetime.datetime.now().replace(microsecond=0)
    time_key = start_time.strftime('%Y_%m_%d_%H_%M_%S')
    lgr.info('time_key {0}'.format(time_key))

    # Splitting the file name
    file_name_only = file_name.split('\\')
    filename_noext = file_name_only[-1].replace('.txt', '')

    # #########################################################
    # # Before starting, go and rename from the pyshare folder
    # # any output files that contain the name of the file that
    # # we are going to process, this is a necessary step if we
    # # are using a file that was processed before
    # #########################################################
    # # Splitting the file name
    # file_name_only = file_name.split('\\')
    # filename_noext = file_name_only[-1].replace('.txt', '')
    #
    # lgr.info('rename wav files in {0}'.format(pyshare_folder))
    # for item in os.listdir(pyshare_folder):
    #     base, ext = os.path.splitext(item)
    #     lgr.info('base: {0}, ext: {1}'.format(base, ext))
    #     if ext == '.wav' and filename_noext in base:
    #         s = os.path.join(pyshare_folder, item)
    #         lgr.info("base: {0}".format(base))
    #         d = os.path.join(pyshare_folder, base + '_' + time_key + '.wav')
    #         lgr.info('S: {0}, D: {1}'.format(s, d))
    #         os.rename(s, d)
    #
    # lgr.info('rename lab files in {0}'.format(pyshare_folder + 'labs'))
    # for item in os.listdir(pyshare_folder + 'labs'):
    #     base, ext = os.path.splitext(item)
    #     lgr.info('base: {0}, ext: {1}'.format(base, ext))
    #     if ext == '.lab' and filename_noext in base:
    #         s = os.path.join(pyshare_folder + 'labs', item)
    #         lgr.info("base: {0}".format(base))
    #         d = os.path.join(pyshare_folder, base + '_' + time_key + '.lab')
    #         lgr.info('S: {0}, D: {1}'.format(s, d))
    #         os.rename(s, d)

    #########################################################
    # Before starting, go and remove the files result*.wav
    # from the tomcat folder
    #########################################################

    # lgr.info('rename wav files in {0}'.format(tomcat_folder))
    # for item in os.listdir(tomcat_folder):
    #     base, ext = os.path.splitext(item)
    #     # lgr.info('base: {0}, ext: {1}'.format(base, ext))
    #     if ext == '.wav' and 'result' in base:
    #         s = os.path.join(tomcat_folder, item)
    #         lgr.info("base: {0}".format(base))
    #         d = os.path.join(tomcat_folder + 'old_wav_files', base + '_' + time_key + '.wav')
    #         lgr.info('S: {0}, D: {1}'.format(s, d))
    #         os.rename(s, d)
    #
    # lgr.info('rename lab files in {0}'.format(tomcat_folder + 'lab_files'))
    # for item in os.listdir(tomcat_folder + 'lab_files'):
    #     base, ext = os.path.splitext(item)
    #     # lgr.info('base: {0}, ext: {1}'.format(base, ext))
    #     if ext == '.lab' and filename_noext in base:
    #         s = os.path.join(tomcat_folder + 'lab_files', item)
    #         lgr.info("base: {0}".format(base))
    #         d = os.path.join(tomcat_folder + 'lab_files', base + '_' + time_key + '.lab')
    #         lgr.info('S: {0}, D: {1}'.format(s, d))
    #         os.rename(s, d)

    #########################################################
    # Create a folder according to the txt file name
    #########################################################
    folder_name, ext = os.path.splitext(file_name)
    lgr.info('mibo: folder: {0} ext: {1}'.format(folder_name, ext))
    lgr.info('mibo: all: {0}'.format(testfolder_path))
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
    # Check if folder was created
    if not os.path.exists(folder_name):
        lgr.warn('Directory was not created')
        sys.exit()
    else:
        lgr.info('Directory {0} was created Successfully'.format(folder_name))

    #########################################################
    # Run command line
    #########################################################
    lgr.info('Run command line')

    # command = pm_exe + ' 1 2 9 pmout.txt testFolder/' + file_name + ' 1 y 0 ' + voice
    lgr.info('file_name_only -1: {0}'.format(file_name_only[-1]))
    lgr.info('file_name_only -2: {0}'.format(file_name_only[-2]))
    lgr.info('file_name_only -3: {0}'.format(file_name_only[-3]))

    # for smoketests folder, to process the smoketest files, we need to separate the IDs first
    if input_folder == 'smoketest':
        # Use a temporary file to send the sentences only (without their IDs) to the ProsodyMain
        base, ext = os.path.splitext(file_name_only[-1])  # take the name of the file that we want to process
                                                          # without ext in order to create a temp file
        file_name_noext = file_name_only[-3] + '\\' + file_name_only[-2] + '\\' + base + '.' + "temp.txt"
        lgr.info('text_file: {0}'.format(file_name_noext))

        # Fetch ID numbers for the sentences in the input text file
        input_file = {}
        i = 0;

        lgr.info('testfolder_path: {0}, text_file: {1}'.format(testfolder_path, file_name_noext))
        text_file_original = file_name_only[-3] + '\\' + file_name_only[-2] + '\\' + file_name_only[-1]
        lgr.info('text_file_original: {0}'.format(text_file_original))

        # with open(testfolder_path + text_file) as myfile:
        text_file_temp = open(file_name_noext, "w")
        with open(text_file_original) as myfile:
            for line in myfile:
                number, text = line.partition('\t')[::2]
                text_file_temp.write(text)
                input_file[i] = number
                i += 1

        text_file_temp.close()
        lgr.info('text_file_temp: {0}'.format(file_name_noext))
        command = pm_exe + ' 1 2 4 pmout.txt ' + file_name_noext + ' 1 y 0 ' + voice
    elif input_folder == 'functionaltest':
        command = pm_exe + ' 1 2 4 pmout.txt ' + file_name_only[-3] + '\\' + file_name_only[-2] + '\\' \
                  + file_name_only[-1] + ' 1 y 0 ' + voice


    # command = pm_exe + ' 1 2 9 pmout.txt ' + file_name_only[-3] + '\\' + file_name_only[-2] + '\\'\
    #           + file_name_only[-1] + ' 1 y 0 ' + voice
    try:
        process = subprocess.Popen(command)
        process.wait()
        lgr.info('Command finished: [' + command + ']')
    except:
        lgr.warn('Command failed: [' + command + ']')
        continue

    #########################################################
    # Rename the output files and copy them to their own folder
    #########################################################
    lgr.info('Rename the output files and copy them to their own folder')
    lgr.info('moving wav files in {0} folder to {1}'.format(testfolder_path, folder_name))
    # move wav files in "pyshare" folder to the output folder
    filename_noext = file_name_only[-1].replace('.txt', '')
    i = 0
    testfolder_contain = os.listdir(testfolder_path)
    for item in testfolder_contain:
        base, ext = os.path.splitext(item)
        lgr.info('base: {0}, ext: {1}'.format(base, ext))
        if ext == '.wav' and filename_noext in base:
            s = os.path.join(testfolder_path, item)
            item = item.split('.')
            if input_folder == 'smoketest':
                lgr.info("input_file: {0}".format(input_file))
                lgr.info("input_file[i]: {0}".format(input_file[i]))
                lgr.info('item[-2]: {0}'.format(item[-2].replace('txt', '')))
                new_index = int(item[-2].replace('txt', ''))-1
                lgr.info('new_index: {0}'.format(new_index))
                # file_suffix = '.' + input_file[new_index] + '.' + type_t + '.' + training_version
                file_suffix = '.' + input_file[new_index] + '.' + \
                              ''.join([i for i in synthesize_version if i.isdigit()]) + 'S' + type_t + '.nvc_' + \
                              ''.join([i for i in training_version if i.isdigit()]) + type_t + '_' + training_sentences
            elif input_folder == 'functionaltest':
                # in functionatest we need to take the number of the sentence from the file end and put it in the new one
                lgr.info('item[-2]: {0}'.format(item[-2].replace('txt', '')))
                # lgr.info('item[-2][3:]: {0}'.format(item[-2][3:]))
                # file_suffix = '.' + item[-2].replace('txt', '') + '.' + type_t + '.' + training_version
                file_suffix = '.' + item[-2].replace('txt', '') + \
                              '.' + ''.join([i for i in synthesize_version if i.isdigit()]) + 'S' + type_t + '.nvc_' + \
                              ''.join([i for i in training_version if i.isdigit()]) + type_t + '_' + training_sentences
            d = os.path.join(folder_name, voice_name[:3] + file_suffix + '.wav')
            lgr.info('folder_name: {0}, - voice_name: {1},  - item[0]: {2}, - item[1]: {3}, - file_suffix: {4}'.format(folder_name, voice_name[:3].lower(), item[0], item[1], file_suffix))
            # d = os.path.join(folder_name, item[0] + '.' + item[1] + '.wav')
            lgr.info('S: {0}, D: {1}'.format(s, d))
            shutil.move(s, d)
            i += 1

    #########################################################
    # Copy the lab files to the "testfoler"
    #########################################################
    # lgr.info('Copy the lab files to the "testfoler"')
    # lgr.info('moving wav files in {0} folder to {1}'.format(pyshare_folder+'labs', folder_name))
    # # move wav files in "pyshare" folder to the output folder
    # filename_noext = file_name_only[-1].replace('.txt', '')
    # # i = 0
    # for item in os.listdir(pyshare_folder+'labs'):
    #     base, ext = os.path.splitext(item)
    #     lgr.info('base: {0}, ext: {1}'.format(base, ext))
    #     if ext == '.lab' and filename_noext in base:
    #         s = os.path.join(pyshare_folder+'labs', item)
    #         # item = item.split('.')
    #         # if input_folder == 'smoketest':
    #         #     lgr.info("input_file: {0}".format(input_file))
    #         #     lgr.info("input_file[i]: {0}".format(input_file[i]))
    #         #     file_suffix = '.' + input_file[i] + '.' + type_t + '.' + training_version
    #         # elif input_folder == 'functionaltest':
    #         #     # in smoketest we need to take the number of the sentence from the file end and put it in the new one
    #         #     lgr.info('item[-2]: {0}'.format(item[-2]))
    #         #     file_suffix = '.' + item[-2] + '.' + type_t + '.' + training_version
    #         d = os.path.join(folder_name, item)
    #         # lgr.info(
    #         #     'folder_name: {0}, - voice_name: {1},  - item[0]: {2}, - item[1]: {3}, - file_suffix: {4}'.format(
    #         #         folder_name, voice_name[:3].lower(), item[0], item[1], file_suffix))
    #         # d = os.path.join(folder_name, item[0] + '.' + item[1] + '.wav')
    #         lgr.info('S: {0}, D: {1}'.format(s, d))
    #         shutil.copy2(s, d)
    #         # i += 1
    lgr.info('Copy the lab files to the "testfoler"')
    lgr.info('moving lab files in {0} folder to {1}'.format(tomcat_folder + 'lab_files', folder_name))
    # move wav files in "pyshare" folder to the output folder
    filename_noext = file_name_only[-1].replace('.txt', '')
    i = 0
    for item in os.listdir(tomcat_folder + 'lab_files'):
        base, ext = os.path.splitext(item)
        lgr.info('base: {0}, ext: {1}'.format(base, ext))
        if ext == '.lab' and filename_noext in base:
            s = os.path.join(tomcat_folder + 'lab_files', item)
            item = item.split('.')
            if input_folder == 'smoketest':
                lgr.info("input_file: {0}".format(input_file))
                lgr.info("input_file[i]: {0}".format(input_file[i]))
                file_suffix = '.' + input_file[i] + '.' + type_t + '.' + training_version
            elif input_folder == 'functionaltest':
                # in smoketest we need to take the number of the sentence from the file end and put it in the new one
                lgr.info('item[-3]: {0}'.format(item[-3].replace('txt', '')))
                file_suffix = '.' + item[-3].replace('txt', '') + '.' + type_t + '.' + training_version
            d = os.path.join(folder_name, voice_name[:3] + file_suffix + '.lab')
            lgr.info('folder_name: {0}, - voice_name: {1},  - item[0]: {2}, - item[1]: {3}, - file_suffix: {4}'.format(
                folder_name, voice_name[:3], item[0], item[1], file_suffix))
            # d = os.path.join(folder_name, item[0] + '.' + item[1] + '.wav')
            lgr.info('S: {0}, D: {1}'.format(s, d))
            shutil.copy2(s, d)
            i += 1

    # delete the help file "output_mibo.txt" for smoketest only
    if input_folder == 'smoketest':
        lgr.info('Deleting the help file (with suffix temp) for the smoke test')
        if os.path.exists(file_name_noext):
            os.remove(file_name_noext)
        # Check if file was deleted
        if os.path.exists(file_name_noext):
            lgr.warn('{0} was not deleted'.format(file_name_noext))
        else:
            lgr.info('{0} is deleted'.format(file_name_noext))

    # Time took to create the wav files
    end_time = datetime.datetime.now().replace(microsecond=0)
    # gather the files and their run time to be displayed at the end of the log file
    run_time[file_name] = end_time - start_time
    lgr.info('time taken to process this file: {0}'.format(run_time[file_name]))

# Conclude the proccessed files and it's run time
lgr.info('All files run times:')
for k, v in run_time.items():
    lgr.info('File: {0}, Time: {1}  '.format(k, v))
