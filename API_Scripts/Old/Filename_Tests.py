import urllib2
import unittest
import flask
import Renderer_Functions as Renderer_Functions
import string
from random import randint

class VBFilenameSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Start run test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Completed running test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"
        print self.currentResult

    def test_positive_1_filename(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # parameters
                    template = "NO"
                    user = {"normalization_domain_id": 1, "template": template}
                    resp = Renderer_Functions.filename(resp["cookies"][0], user)

                    # Fetch the info from Database
                    cursor = Renderer_Functions.connect_to_db()
                    cursor.execute("SELECT DISTINCT renderer_text_id, name, text_ssml, normalization_domain_id, text_owner_email, template_id, is_template_org, open_to_public, CASE WHEN open_to_public THEN FALSE  WHEN trim(text_owner_email) ILIKE '" + email + "' THEN TRUE WHEN is_write  THEN TRUE ELSE FALSE END edit_permissions FROM renderer_text LEFT OUTER JOIN renderer_text_permissions USING (renderer_text_id) WHERE (open_to_public  OR trim(text_owner_email) ILIKE '" + email + "' OR trim(user_email) ILIKE '" + email + "') and normalization_domain_id = 1 and template_id IS NULL")

                    db_info = [r for r in cursor]
                    db_info = sorted(db_info, key=lambda k: k[0])

                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    resp = sorted(resp["Results"][1]["list"], key=lambda k: k['Filename_Id'])

                    # Choose a random row to test
                    random_line = randint(0, len(db_info) - 1)

                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp_status_code, 200)) is None \
                            and (self.assertEqual(resp[random_line]["text_ssml"], db_info[random_line][2]) is None) \
                            and (self.assertEqual(resp[random_line]["Normalization_Domain_Id"], str(db_info[random_line][3])) is None) \
                            and (self.assertEqual(resp[random_line]["Filename_Id"], str(db_info[random_line][0])) is None) \
                            and (self.assertEqual(resp[random_line]["Text_Owner_Email"], db_info[random_line][4]) is None) \
                            and (self.assertEqual(resp[random_line][" open_to_public"], db_info[random_line][7]) is None) \
                            and (self.assertEqual(resp[random_line]["Name"], db_info[random_line][1]) is None):
                        print "Filename Pass"
                        print "resp: {0}".format(resp)

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_2_filename_wrong_normalization_domain_id(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # parameters
                    template = "NO"
                    user = {"normalization_domain_id": "abc", "template": template}
                    resp = Renderer_Functions.filename(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 400)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Incorrect value for normalization_domain_id.") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_3_filename_missing_normalization_domain_id(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # parameters
                    template = "NO"
                    user = {"template": template}
                    resp = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp["Results"][1]["list"]

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 400)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Missing Parameter normalization_domain_id.") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_4_filename_wrong_login(self):
                    # parameters
                    template = "NO"
                    user = {"normalization_domain_id": 1, "template": template}
                    resp = Renderer_Functions.filename("123456789", user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

    def test_negative_filename_5_missing_login(self):
                    # parameters
                    template = "NO"
                    user = {"normalization_domain_id": 1, "template": template}
                    resp = Renderer_Functions.filename("NO TOKEN", user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

if __name__ == '__main__':
    print "-------------Test Result----------------\n"
    testResult = unittest.main(verbosity=1)
