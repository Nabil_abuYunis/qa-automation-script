import urllib2
import unittest
import flask
import Renderer_Functions as Renderer_Functions
import random
import string


class VBSignupSuites(unittest.TestCase):

    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Start run test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Completed running test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"
        print self.currentResult

    def test_positive_1_signup(self):
        #  Parameters
        random_email = ''.join(random.choice(string.letters) for ii in range(5)) + '@speechmorphing.com'
        print "random_email: {0}".format(random_email)
        email = random_email
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email_address": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name, "middle_name": "koko", "country": "us"}
        resp = Renderer_Functions.signup(user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["User"]["email_address"], email) is None) \
                        and (self.assertEqual(resp["User"]["username"], username) is None) \
                        and (self.assertEqual(resp["User"]["first_name"], first_name) is None) \
                        and (self.assertEqual(resp["User"]["last_name"], last_name) is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "Status code: {0}".format(resp["status_code"])
                    print "email_address: {0}".format(resp["User"]["email_address"])
                    print "username: {0}".format(resp["User"]["username"])
                    print "first_name: {0}".format(resp["User"]["first_name"])
                    print "last_name: {0}".format(resp["User"]["last_name"])

            except urllib2.URLError as e:   # if response code is 200 but the fetched email is
                                            # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_2_signup_invalid_email(self):
        # Parameters
        random_email = ''.join(random.choice(string.letters) for ii in range(5))
        print "random_email: {0}".format(random_email)
        email = random_email
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email_address": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["Status Code"] != 200:
            try:
                if (self.assertEqual(resp["Status Code"], 400)) is None \
                        and (self.assertEqual(resp["Error Message"]["details"],
                                              "email_address: invalid email address") is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["Status Code"])
                    print "Reason: {0}".format(resp["Error Message"]["details"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] == 200:
            print "Negative Test Failed"

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_3_signup_missing_email(self):
        # Parameters
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["Status Code"] != 200:
            try:
                if (self.assertEqual(resp["Status Code"], 400)) is None \
                        and (self.assertEqual(resp["Error Message"],
                                              "Missing Parameter email_address.") is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["Status Code"])
                    print "Reason: {0}".format(resp["Error Message"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] == 200:
            print "Negative Test Failed"

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_4_signup_empty_email(self):
        # Parameters
        email = ""
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email_address": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["Status Code"] != 200:
            try:
                if (self.assertEqual(resp["Status Code"], 400)) is None \
                        and (self.assertEqual(resp["Error Message"],
                                              "Email cannot be blank  or missing parameter.") is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["Status Code"])
                    print "Reason: {0}".format(resp["Error Message"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] == 200:
            print "Negative Test Failed"

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_5_signup_invalid_password(self):
        # Parameters
        random_email = ''.join(random.choice(string.letters) for ii in range(5)) + '@speechmorphing.com'
        print "random_email: {0}".format(random_email)
        email = random_email
        password = "abc"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email_address": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["Status Code"], 400)) is None \
                        and (self.assertEqual(resp["Error Message"]["details"],
                                              "password: too short") is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["Status Code"])
                    print "Reason: {0}".format(resp["Error Message"]["details"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] == 200:
            print "Negative Test Failed"

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_6_signup_missing_password(self):
        # Parameters
        random_email = ''.join(random.choice(string.letters) for ii in range(5)) + '@speechmorphing.com'
        print "random_email: {0}".format(random_email)
        email = random_email
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email_address": email, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["Status Code"] != 200:
            try:
                if (self.assertEqual(resp["Status Code"], 400)) is None \
                        and (self.assertEqual(resp["Error Message"],
                                              "Missing Parameter password.") is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["Status Code"])
                    print "Reason: {0}".format(resp["Error Message"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] == 200:
            print "Negative Test Failed"

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_7_signup_empty_password(self):
        # Parameters
        random_email = ''.join(random.choice(string.letters) for ii in range(5)) + '@speechmorphing.com'
        print "random_email: {0}".format(random_email)
        email = random_email
        password = ""
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email_address": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["Status Code"] != 200:
            try:
                if (self.assertEqual(resp["Status Code"], 400)) is None \
                        and (self.assertEqual(resp["Error Message"],
                                              "Password too short.") is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["Status Code"])
                    print "Reason: {0}".format(resp["Error Message"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] == 200:
            print "Negative Test Failed"

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_8_signup_empty_username(self):
        # Parameters
        random_email = ''.join(random.choice(string.letters) for ii in range(5)) + '@speechmorphing.com'
        print "random_email: {0}".format(random_email)
        email = random_email
        password = "abc123"
        username = ""
        last_name = "test123"
        first_name = "test123"
        user = {"email_address": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["Status Code"] != 200:
            try:
                if (self.assertEqual(resp["Status Code"], 400)) is None \
                        and (self.assertEqual(resp["Error Message"],
                                              "User Name cannot be blank  or missing parameter.") is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["Status Code"])
                    print "Reason: {0}".format(resp["Error Message"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] == 200:
            print "Negative Test Failed"

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_9_signup_missing_username(self):
        # Parameters
        random_email = ''.join(random.choice(string.letters) for ii in range(5)) + '@speechmorphing.com'
        print "random_email: {0}".format(random_email)
        email = random_email
        password = "abc123"
        last_name = "test123"
        first_name = "test123"
        user = {"email_address": email, "password": password, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["Status Code"] != 200:
            try:
                if (self.assertEqual(resp["Status Code"], 400)) is None \
                        and (self.assertEqual(resp["Error Message"],
                                              "Missing Parameter username.") is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["Status Code"])
                    print "Reason: {0}".format(resp["Error Message"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] == 200:
            print "Negative Test Failed"

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_10_signup_empty_lastname(self):
        # Parameters
        random_email = ''.join(random.choice(string.letters) for ii in range(5)) + '@speechmorphing.com'
        print "random_email: {0}".format(random_email)
        email = random_email
        password = "abc123"
        username = "vghung"
        last_name = ""
        first_name = "test123"
        user = {"email_address": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["Status Code"] != 200:
            try:
                if (self.assertEqual(resp["Status Code"], 400)) is None \
                        and (self.assertEqual(resp["Error Message"],
                                              "Last Name cannot be blank  or missing parameter.") is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["Status Code"])
                    print "Reason: {0}".format(resp["Error Message"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] == 200:
            print "Negative Test Failed"

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_11_signup_missing_lastname(self):
        # Parameters
        random_email = ''.join(random.choice(string.letters) for ii in range(5)) + '@speechmorphing.com'
        print "random_email: {0}".format(random_email)
        email = random_email
        password = "abc123"
        username = "vghung"
        first_name = "test123"
        user = {"email_address": email, "password": password, "username": username,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["Status Code"] != 200:
            try:
                if (self.assertEqual(resp["Status Code"], 400)) is None \
                        and (self.assertEqual(resp["Error Message"],
                                              "Last Name cannot be blank null or missing parameter.") is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["Status Code"])
                    print "Reason: {0}".format(resp["Error Message"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] == 200:
            print "Negative Test Failed"

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_12_signup_empty_firstname(self):
        # Parameters
        random_email = ''.join(random.choice(string.letters) for ii in range(5)) + '@speechmorphing.com'
        print "random_email: {0}".format(random_email)
        email = random_email
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = ""
        user = {"email_address": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["Status Code"] != 200:
            try:
                if (self.assertEqual(resp["Status Code"], 400)) is None \
                        and (self.assertEqual(resp["Error Message"],
                                              "First Name cannot be blank  or missing parameter.") is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["Status Code"])
                    print "Reason: {0}".format(resp["Error Message"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] == 200:
            print "Negative Test Failed"

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_13_signup_missing_firstname(self):
        # Parameters
        random_email = ''.join(random.choice(string.letters) for ii in range(5)) + '@speechmorphing.com'
        print "random_email: {0}".format(random_email)
        email = random_email
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        user = {"email_address": email, "password": password, "username": username, "last_name": last_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["Status Code"] != 200:
            try:
                if (self.assertEqual(resp["Status Code"], 400)) is None \
                        and (self.assertEqual(resp["Error Message"],
                                              "Missing Parameter first_name.") is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["Status Code"])
                    print "Reason: {0}".format(resp["Error Message"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] == 200:
            print "Negative Test Failed"

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

if __name__ == '__main__':
    print "-------------Test Result----------------\n"
    testResult = unittest.main(verbosity=1)
