import urllib2
import unittest
import flask
import Renderer_Functions as Renderer_Functions
import string

class VBSaySuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Start run test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Completed running test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"
        print self.currentResult

    def test_positive_1_say(self):

        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    smi_voice_id = "72057594037927940"
                    request_id = "215020"
                    domain_id = "1"
                    text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big</speak>"
                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "domain_id": domain_id,
                            "text_ssml": text_ssml}
                    resp = Renderer_Functions.say(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp["status_code"], 200)) is None \
                            and (self.assertEqual(resp["Results"]["Filename"], "http://54.87.242.36:8080/static/abc_72057594037927940_215020.wav")) is None:
                        print "Get Voice Pass"
                        print "resp: {0}".format(resp)
                        print "status_code: {0}".format(resp["status_code"])
                        print "Filename: {0}".format(resp["Results"]["Filename"])

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_2_say_wrong_smi_voice_id(self):

        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    smi_voice_id = "ABCD"
                    request_id = "215020"
                    domain_id = "1"
                    text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "domain_id": domain_id,
                            "text_ssml": text_ssml}
                    resp = Renderer_Functions.say(resp["cookies"][0], user)

                    # here we will check if the voice detail is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 400)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Incorrect value for smi_voice_id.") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_3_say_missing_smi_voice_id(self):

        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    request_id = "215020"
                    domain_id = "1"
                    text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
                    user = {"request_id": request_id, "domain_id": domain_id,
                            "text_ssml": text_ssml}
                    resp = Renderer_Functions.say(resp["cookies"][0], user)

                    # here we will check if the voice detail is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 400)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Missing Parameter smi_voice_id") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_4_say_wrong_request_id(self):

        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    smi_voice_id = "72057594037927940"
                    request_id = "ABCD"
                    domain_id = "1"
                    text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "domain_id": domain_id,
                            "text_ssml": text_ssml}
                    resp = Renderer_Functions.say(resp["cookies"][0], user)

                    # here we will check if the voice detail is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 400)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Incorrect value for request_id.") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_5_say_missing_request_id(self):

        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    smi_voice_id = "72057594037927940"
                    domain_id = "1"
                    text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
                    user = {"smi_voice_id": smi_voice_id, "domain_id": domain_id,
                            "text_ssml": text_ssml}
                    resp = Renderer_Functions.say(resp["cookies"][0], user)

                    # here we will check if the voice detail is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 400)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Missing Parameter request_id") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_6_say_wrong_domain_id(self):

        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    smi_voice_id = "72057594037927940"
                    request_id = "215020"
                    domain_id = "A"
                    text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "domain_id": domain_id,
                            "text_ssml": text_ssml}
                    resp = Renderer_Functions.say(resp["cookies"][0], user)

                    # here we will check if the voice detail is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 400)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Incorrect value for domain_id.") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_7_say_missing_domain_id(self):

        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    smi_voice_id = "72057594037927940"
                    request_id = "215020"
                    text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id,
                            "text_ssml": text_ssml}
                    resp = Renderer_Functions.say(resp["cookies"][0], user)

                    # here we will check if the voice detail is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 400)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Missing Parameter domain_id") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_8_say_wrong_text_sml(self):

        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    smi_voice_id = "72057594037927940"
                    request_id = "215020"
                    domain_id = "1"
                    text_ssml = ""
                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "domain_id": domain_id,
                            "text_ssml": text_ssml}
                    resp = Renderer_Functions.say(resp["cookies"][0], user)

                    # here we will check if the voice detail is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 400)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Incorrect value for smi_voice_id.") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_9_say_missing_text_ssml(self):

        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    smi_voice_id = "72057594037927940"
                    request_id = "215020"
                    domain_id = "1"
                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "domain_id": domain_id}
                    resp = Renderer_Functions.say(resp["cookies"][0], user)

                    # here we will check if the voice detail is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 400)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Missing Parameter text_ssml.") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_10_say_wrong_login(self):
                    # Parameters
                    smi_voice_id = "72057594037927940"
                    request_id = "215020"
                    domain_id = "1"
                    text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "domain_id": domain_id,
                            "text_ssml": text_ssml}
                    resp = Renderer_Functions.say("123456789", user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

    def test_negative_11_say_missing_login(self):
                    # Parameters
                    smi_voice_id = "72057594037927940"
                    request_id = "215020"
                    domain_id = "1"
                    text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "domain_id": domain_id,
                            "text_ssml": text_ssml}
                    resp = Renderer_Functions.say("NO TOKEN", user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

if __name__ == '__main__':
    print "-------------Test Result----------------\n"
    testResult = unittest.main(verbosity=1)
