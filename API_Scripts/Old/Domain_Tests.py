import urllib2
import unittest
import flask
import Renderer_Functions as Renderer_Functions
import string
from random import randint

class VBDomainSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Start run test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Completed running test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"
        print self.currentResult

    def test_positive_1_domain(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if self.assertEqual(email_address, email) is None:
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    # Get Random Language id
                    cursor = Renderer_Functions.connect_to_db()
                    # select only the langauge_id's that have domains
                    cursor.execute('SELECT DISTINCT language_id FROM smi_domain where supported = true')
                    db_info = [r for r in cursor]
                    random_line = randint(0, cursor.rowcount-1)

                    language_id = db_info[random_line][0]

                    # Send to Domain
                    user = {"language_id": language_id, "sendUnsupported": True}
                    resp = Renderer_Functions.domain(resp["cookies"][0], user)

                    # Fetch the info from the Age table
                    cursor = Renderer_Functions.connect_to_db()
                    cursor.execute("WITH webSupported as (SELECT DISTINCT smi_domain_id FROM canned_clip_website WHERE supported)SELECT smi_domain_id,domain_name as name, language_id,  normalization_domain_id, style_id, mood_id, speed, pitch, volume, open_to_public, supported,  smi_domain_owner_email, parent_project_id::text, string_agg(keyword, ', ') as keywords, string_agg(user_email, ', ') as email, websupported.smi_domain_id is not NULL web_supported   FROM smi_domain LEFT OUTER JOIN smi_domain_permissions USING (smi_domain_id) LEFT OUTER JOIN smi_domain_keywords USING (smi_domain_id) LEFT OUTER JOIN websupported using (smi_domain_id) WHERE ( open_to_public OR  smi_domain_owner_email = '" + email + "'  OR user_email ILIKE '" + email + "')and marked_for_deletion is NULL  and  supported = true  AND CASE WHEN (length('') = 0  OR trim(domain_name) ~* '()' OR smi_domain_id IN (SELECT smi_domain_id FROM smi_domain_keywords  WHERE  keyword ~* '^()$')) THEN TRUE ELSE FALSE END GROUP BY smi_domain_id, domain_name, language_id, parent_id, normalization_domain_id, style_id, mood_id, speed, pitch, volume, open_to_public, smi_domain_owner_email, websupported.smi_domain_id   ORDER BY supported DESC, domain_name")

                    db_info = [r for r in cursor]
                    db_info = sorted(db_info, key=lambda k: k[0])
                    db_info = [x for x in db_info if x[2] == language_id]

                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    resp = sorted(resp["results"], key=lambda k: k['smi_domain_id'])

                    # Choose a random row to test
                    random_line = randint(0, len(db_info) - 1)

                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp_status_code, 200) is None
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["smi_domain_id"], str(db_info[random_line][0])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["name"], str(db_info[random_line][1])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["mood_id"], str(db_info[random_line][5])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["open_to_public"], str(db_info[random_line][9])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["pitch"], str(db_info[random_line][7])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["smi_domain_owner_email"], str(db_info[random_line][11])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["supported"], str(db_info[random_line][10])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["parent_project_id"], str(db_info[random_line][12])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["volume"], str(db_info[random_line][8])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["style_id"], str(db_info[random_line][4])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["speed"], str(db_info[random_line][6])))):
                                print "Domain Pass"
                                print "resp: {0}".format(resp)
                    else:
                        print "Domain Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_2_domain_incorrect_language_id(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if self.assertEqual(email_address, email) is None:
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    language_id = "a"
                    user = {"language_id": language_id, "sendUnsupported": True}
                    resp = Renderer_Functions.domain(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            #if (self.assertEqual(resp["status_code"], 400)) is None \
                            #       and (self.assertEqual(resp["details"],
                            #                            "Incorrect value for language_id.") is None):
                            if (self.assertEqual(resp["results"]["error message"],
                                                          "Incorrect value for language_id.") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["results"]["error message"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] != 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_3_domain_missing_parameters(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if self.assertEqual(email_address, email) is None:
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    user = {}
                    resp = Renderer_Functions.domain(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if self.assertEqual(resp["results"]["error message"],
                                                          "Missing Parameter language_id.") is None:
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["results"]["error message"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] != 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_4_domain_missing_language_id(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if self.assertEqual(email_address, email) is None:
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    user = {"sendUnsupported": True}
                    resp = Renderer_Functions.domain(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if self.assertEqual(resp["results"]["error message"],
                                                          "Missing Parameter language_id.") is None:
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["results"]["error message"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] != 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_5_domain_missing_sendUnsupported(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if self.assertEqual(email_address, email) is None:
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    user = {"language_id": 2}
                    resp = Renderer_Functions.domain(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if self.assertEqual(resp["results"]["error message"],
                                                          "Missing Parameter sendUnsupported.") is None:
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["results"]["error message"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] != 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_6_domain_wrong_login(self):
                    # Parameters
                    user = {"language_id": 2, "sendUnsupported": True}
                    resp = Renderer_Functions.domain("123456789", user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "unauthorized access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] != 200:
                        print "Negative Test Failed"

    def test_negative_7_domain_missing_login(self):
                    # Parameters
                    user = {"language_id": 2, "sendUnsupported": True}
                    resp = Renderer_Functions.domain("NO TOKEN", user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "unauthorized access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] != 200:
                        print "Negative Test Failed"

if __name__ == '__main__':
    print "-------------Test Result----------------\n"
    testResult = unittest.main(verbosity=1)
