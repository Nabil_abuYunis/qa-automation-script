import unittest
import Renderer_Functions
from Database import Postgres
import random
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBuserPermissionSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}
    # Read config file
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)


    def test_positive_1_userPermission(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Get account id
        # user = {}
        # resp = Renderer_Functions.send_request(cookie, user, "accountlist")
        # # Get account that have projects within from sql instead of API
        cursor = Postgres.connect_to_db()
        # email = self.config.get('LOGIN', 'email')
        # cursor.execute("SELECT distinct(account_id) FROM user_account_permissions where user_email like 'admin@speechmorphing.com' "
        #                "or user_email like '" + email + "'" + " in (select distinct account_id from project) "
        #                 "intersect (select distinct account_id from project)")
        # db_info = [r for r in cursor]
        # # random_line = randint(0, resp["count"] - 1)
        # # account_id = resp["results"][random_line]["account_id"]
        # random_account = random.choice(db_info)
        # random_account = 'S4398046511105'

        # Parameters
        allowed_accounts = Renderer_Functions.get_allowed_accounts(email)
        random_account = random.choice(allowed_accounts)

        user = {"account_id": random_account[0], "user_email": email}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "userPermission")

        # Fetch projects from the Database for the same random account id
        query = "select * from user_project_permissions where user_email ilike '" + email + "' and project_id ilike '%" + random_account[0] + "%'"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[1])

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"][0]["user_email"], db_info[0][0]) is None) \
                and (self.assertEqual(resp["results"][0]["project_id"], db_info[0][1]) is None) \
                and (self.assertEqual(resp["results"][0]["project_permission_id"], str(db_info[0][2])) is None) :
            print ("Project List Pass")
            print("resp: {0}".format(resp))
            print("user_email: {0}".format(db_info[0][0]))
            print("project_id: {0}".format(db_info[0][1]))
            print("project_permission_id: {0}".format(db_info[0][2]))
        else:
            print("Positive Test Failed")
            assert False

    print("User Permission Pass")

    def test_negative_2_userPermission_wrong_login(self):
        email = self.config["LOGINADMIN"]["email"]

        # Parameters
        allowed_accounts = Renderer_Functions.get_allowed_accounts(email)
        random_account = random.choice(allowed_accounts)

        user = {"account_id": random_account[0], "user_email": email}
        resp = Renderer_Functions.send_request("123456789", user, "userPermission")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_userPermission_missing_login(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        allowed_accounts = Renderer_Functions.get_allowed_accounts(email)
        random_account = random.choice(allowed_accounts)

        user = {"send_unsupported": True, "account_id": random_account[0]}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "userPermission")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_userPermission_expired_token(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        allowed_accounts = Renderer_Functions.get_allowed_accounts(email)
        random_account = random.choice(allowed_accounts)

        user = {"send_unsupported": True, "account_id": random_account[0]}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "userPermission")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_5_user_without_permission(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        # allowed_accounts = Renderer_Functions.get_allowed_accounts(email)
        # random_account = random.choice(allowed_accounts)

        cursor = Postgres.connect_to_db()
        query = "SELECT account_id FROM user_account_permissions INNER JOIN account USING(account_id) " \
                "INNER JOIN user_permissions ap USING(user_permission_id) WHERE(user_email  not ILIKE '" + email + "')except" \
                " SELECT account_id FROM user_account_permissions INNER JOIN account USING(account_id) INNER JOIN user_permissions ap USING(user_permission_id) " \
                " WHERE(user_email  ILIKE '" + email + "')"
        cursor.execute(query)
        db_info = [r for r in cursor]
        random_account = random.choice(db_info[0])

        user = {"account_id": random_account, "user_email": email}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "userPermission")

        if resp["status_code"] == 200 \
                and (self.assertEqual(resp["results"]["error_message"], 'you do not have permission to see users permissions.') is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["results"]["error_message"]))

            # Check error type
        else:
            print("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
