import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from Database import Postgres
import random
import string

class VBWordReplacementAddSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    def test_positive_0_add_true(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]

        # Parameters
        cursor = Postgres.connect_to_db()
        # Select smi_domain_id and their gestures gestures
        query = "select smi_domain_id, gesture_id from smi_domain_allowed_gestures where gesture_id in (select gesture_id from gesture where supported AND open_to_public) " \
                + "AND smi_domain_id in (select smi_domain_id from smi_domain where supported AND open_to_public) ORDER BY smi_domain_id "
        cursor.execute(query)
        db_info = [r for r in cursor]
        # Get random smi_domain_id and it's gesture
        random_line = random.choice(db_info)

        smi_domain_id = random_line[0]
        gesture_id = random_line[1]

        #Parameters
        word = ''.join(random.choice(string.ascii_letters) for i in range(5))
        use_gesture = True

        user = {"smi_domain_id": smi_domain_id, "use_gesture": use_gesture, "word": word, "gesture_id": gesture_id}
        resp = Renderer_Functions.word_replacement_add(cookies, user)

        if (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["success_message"], "word '"+word+"' will be replaced with gesture_id = "+str(gesture_id)) is None):
            print("Word Replacement Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))
            print("success message: {0}".format(resp['results']["success_message"]))

        # After adding the word, do remove them in order to leave the DB as it was
        user = {"smi_domain_id": smi_domain_id, "word": word}
        Renderer_Functions.word_replacement_del(cookies, user)

    def test_positive_1_add_false(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]

        # Parameters
        cursor = Postgres.connect_to_db()
        # Get random smi_domain_id
        query = "select smi_domain_id, gesture_id from smi_domain_allowed_gestures where gesture_id in (select gesture_id from gesture where supported AND open_to_public) " \
                + "AND smi_domain_id in (select smi_domain_id from smi_domain where supported AND open_to_public) ORDER BY smi_domain_id "
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])

        word = ''.join(random.choice(string.ascii_letters) for i in range(5))
        use_gesture = False
        replacement_word = ''.join(random.choice(string.ascii_letters) for i in range(5))

        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}

        resp = Renderer_Functions.word_replacement_add(cookies, user)

        if (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["success_message"], "word '"+word+"' will be replaced with '"+replacement_word+"'") is None):
            print("Word Replacement Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))
            print("success message: {0}".format(resp['results']["success_message"]))

        # After adding the word, do remove them in order to leave the DB as it was
        user = {"smi_domain_id": smi_domain_id, "word": word}
        Renderer_Functions.word_replacement_del(cookies, user)

    def test_negative_2_incorrect_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        smi_domain_id = "chest"
        word = "hello"
        use_gesture = False
        replacement_word = "world"

        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}

        resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Incorrect value for smi_domain_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_3_incorrect_word(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        smi_domain_id = "72057594037927948"
        word = 123456
        use_gesture = False
        replacement_word = "world"

        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}

        resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Incorrect value for word.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_4_incorrect_use_gesture(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        smi_domain_id = "72057594037927948"
        word = "dance"
        use_gesture = 789456
        replacement_word = "world"

        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}

        resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Incorrect value for use_gesture.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_5_incorrect_gesture_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        smi_domain_id = "72057594037927948"
        word = "dance"
        use_gesture = True
        gesture_id = "not all of them"
        replacement_word = "world"

        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "gesture_id":gesture_id}
        resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Incorrect value for gesture_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_6_incorrect_replacement(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        smi_domain_id = "72057594037927948"
        word = "dance"
        use_gesture = False
        replacement_word = 123456

        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}
        resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Incorrect value for replacement_word.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_7_missing_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        word = "dance"
        use_gesture = False
        replacement_word = "world"

        user = {"word": word, "use_gesture":use_gesture, "replacement_word":replacement_word}

        resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Missing Parameter smi_domain_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_8_missing_word(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        smi_domain_id = "72057594037927948"
        use_gesture = False
        gesture_id = 2
        replacement_word = "world"

        user = {"smi_domain_id": smi_domain_id,"use_gesture":use_gesture, "replacement_word":replacement_word}

        resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Missing Parameter word.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_9_missing_use_gesture(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        smi_domain_id = "72057594037927948"
        word = "dance"
        gesture_id = 2
        replacement_word = "world"

        user = {"smi_domain_id": smi_domain_id, "word": word, "replacement_word":replacement_word}

        resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Missing Parameter use_gesture.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_10_missing_gesture_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        smi_domain_id = "72057594037927948"
        word = "dance"
        use_gesture = True

        user = {"smi_domain_id": smi_domain_id, "use_gesture": use_gesture, "word": word}
        resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Missing Parameter gesture_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_11_missing_replacement(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        smi_domain_id = "72057594037927948"
        word = "dance"
        use_gesture = False

        user = {"smi_domain_id": smi_domain_id, "use_gesture": use_gesture, "word": word}
        resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Missing Parameter replacement_word.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_14_permission_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        cursor = Postgres.connect_to_db()
        # Select smi_domain_id and their gestures gestures
        query = "select smi_domain_id from smi_domain where supported = False"
        cursor.execute(query)
        db_info = [r for r in cursor]

        smi_domain_id = random.choice(db_info)[0]
        word = "dance"
        use_gesture = False
        replacement_word = "world"

        user = {"smi_domain_id": smi_domain_id, "word":word, "use_gesture": use_gesture,
                "replacement_word": replacement_word}
        resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            print(resp['results']["error message"])
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],"you do not have permission to use this domain") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_15_permission_gesture(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        smi_domain_id = "72057594037927948"
        word = "dance"
        use_gesture = True

        cursor = Postgres.connect_to_db()
        # Select smi_domain_id and their gestures gestures
        query = "select gesture_id from gesture where supported = False"
        cursor.execute(query)
        db_info = [r for r in cursor]
        gesture_id = random.choice(db_info)[0]

        user = {"smi_domain_id": smi_domain_id, "word":word, "use_gesture": use_gesture, "gesture_id": gesture_id}
        resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "you do not have persmission to use this gesture for word replacement.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_6_word_replacement_wrong_login(self):
        # parameters
        smi_domain_id = "72057594037927948"
        word = "hello"
        use_gesture = False
        gesture_id = 2
        replacement_word = "ageag"

        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}

        resp = Renderer_Functions.word_replacement("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_11_expired_token(self):
        # parameters
        smi_domain_id = "72057594037927948"
        word = "hello"
        use_gesture = False
        replacement_word = "ageag"

        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}
        
        resp = Renderer_Functions.word_replacement_add("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_7_word_replacement_missing_login(self):
        # parameters
        smi_domain_id = "72057594037927948"
        word = "hello"
        use_gesture = False
        gesture_id = 2
        replacement_word = "ageag"

        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}
        
        resp = Renderer_Functions.word_replacement("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_16_already_exists(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]
        # Parameters

        # Send first the word - replacement

        cursor = Postgres.connect_to_db()
        # Select smi_domain_id and their gestures gestures
        query = "select smi_domain_id, gesture_id from smi_domain_allowed_gestures where gesture_id in (select gesture_id from gesture where supported AND open_to_public) " \
                + "AND smi_domain_id in (select smi_domain_id from smi_domain where supported AND open_to_public) ORDER BY smi_domain_id "
        cursor.execute(query)
        db_info = [r for r in cursor]
        # Get random smi_domain_id and it's gesture
        random_line = random.choice(db_info)

        smi_domain_id = random_line[0]
        gesture_id = random_line[1]

        # Parameters
        word = ''.join(random.choice(string.ascii_letters) for i in range(5))
        use_gesture = True

        user = {"smi_domain_id": smi_domain_id, "use_gesture": use_gesture, "word": word, "gesture_id": gesture_id}
        resp = Renderer_Functions.word_replacement_add(cookies, user)

        # ----------------------
        # Send again the same first word - replacement

        resp = Renderer_Functions.word_replacement_add(cookies, user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              " " + word + " already exists.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

        # After adding the word, do remove them in order to leave the DB as it was
        user = {"smi_domain_id": smi_domain_id, "word": word}
        Renderer_Functions.word_replacement_del(cookies, user)

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)