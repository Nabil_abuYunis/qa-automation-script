import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint
from Database import Postgres

class VBVoiceDetailsSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    def test_positive_1_voice_details(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        # smi_voice_id = "72057594037928282"
        user = {"send_unsupported": True}
        respVoices = Renderer_Functions.get_voices(resp["cookies"][0], user)
        random_line = randint(0, int(respVoices['count']) - 1)
        smi_voice_id = respVoices['results'][random_line]["smi_voice_id"]

        user = {"smi_voice_id": smi_voice_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "voicedetails")
        print(resp)
        resp_results = resp["results"][0]

        # Fetch the info from Database
        cursor = Postgres.connect_to_db()
        cursor.execute("SELECT name, gender_id, age_id, language_id, img_url FROM smi_voice  where smi_voice_id = " + smi_voice_id)

        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        # resp = sorted(resp["Results"], key=lambda k: k['Mood_Id'])

        # Choose a random row to test
        random_line = randint(0, len(db_info) - 1)
        print(db_info)
        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp_results["gender_id"], db_info[random_line][1])) is None \
                and (self.assertEqual(resp_results["age_id"], db_info[random_line][2])) is None \
                and (self.assertEqual(resp_results["name"], db_info[random_line][0])) is None \
                and (self.assertEqual(resp_results["language_id"], db_info[random_line][3])) is None:
            print("Voice Details Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp_status_code))
            print("gender_id: {0}".format(resp_results["gender_id"]))
            print("age_id: {0}".format(resp_results["age_id"]))
            print("name: {0}".format(resp_results["name"]))
            print("language_id: {0}".format(resp_results["language_id"]))

    def test_negative_2_voice_details_wrong_login(self):
        # Parameters
        user = {"smi_voice_id": "72057594037927938"}
        resp = Renderer_Functions.send_request("123456789", user, "voicedetails")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_8_expired_token(self):
        # Parameters
        user = {"smi_voice_id": "72057594037927938"}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "voicedetails")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_3_voice_details_missing_login(self):
        # Parameters
        smi_voice_id = "72057594037927938"
        user = {"smi_voice_id": smi_voice_id}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "voicedetails")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_5_voice_details_incorrect_value(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        smi_voice_id = "abcvsdag"
        user = {"smi_voice_id": smi_voice_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "voicedetails")
        print(resp)
        #resp_results = resp["results"][0]

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for smi_voice_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_6_voice_details_missing_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "voicedetails")
        print(resp)
        #resp_results = resp["results"][0]

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter smi_voice_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_7_voice_details_voice_accessible(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        smi_voice_id = "1111111111111"
        user = {"smi_voice_id": smi_voice_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "voicedetails")
        print(resp)
        #resp_results = resp["results"][0]

        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], 401)) is None \
                        and (self.assertEqual(resp["details"],
                                              "unauthorized access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)