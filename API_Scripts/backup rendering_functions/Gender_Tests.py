import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from Database import Postgres


class VBGenderSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Start run test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Completed running test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")
        print( self.currentResult)

    def test_positive_1_gender(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.gender(resp["cookies"][0], user)

        # Fetch the info from Database
        cursor = Postgres.connect_to_db()
        cursor.execute("WITH webSupported as (SELECT DISTINCT gender_id FROM smi_voice where smi_voice_id IN (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)) select gender_id, name, supported, (websupported.gender_id is not NULL OR gender_id = 0) web_supported from gender LEFT OUTER JOIN websupported using (gender_id) ORDER BY (websupported.gender_id is not NULL OR gender_id = 0) DESC, gender.gender_id ")

        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k['gender_id'])

        # Choose a random row to test
        # random_line = randint(0, len(db_info) - 1)
        # print(resp[random_line])
        # print(db_info[random_line])

        count = resp.__len__()
        for x in range(count - 1):
            # here we will check if the request is passed or failed
            #and (self.assertEqual(resp[random_line]['img_url'], db_info[random_line][3]) is None) \
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[x]['gender_id'], db_info[x][0]) is None) \
                    and (self.assertEqual(resp[x]["supported"], db_info[x][2]) is None) \
                    and (self.assertEqual(resp[x]["name"], db_info[x][1]) is None):
                print( "Gender Pass")
                print( "resp: {0}".format(resp))
                print( "Gender_Id: {0}".format(resp[x]['gender_id']))
                print( "Supported: {0}".format(resp[x]["supported"]))
                print( "Name: {0}".format(resp[x]["name"]))
                #print( "IMG_Url: {0}".format(resp[random_line]["img_url"]))

    def test_negative_2_gender_missing_sendUnsupported(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {}
        resp = Renderer_Functions.gender(resp["cookies"][0], user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter send_unsupported.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_3_gender_wrong_login(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.gender("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_4_gender_missing_login(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.gender("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_5_gender_incorrect_sendUnsupported(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {"send_unsupported" : "news"}
        resp = Renderer_Functions.gender(resp["cookies"][0], user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for send_unsupported.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.age("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

if __name__ == '__main__':
    print( "-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)