from unittest import TestLoader, TestSuite
from HtmlTestRunner import HTMLTestRunner
import glob
import inspect
import importlib

# 1. IMPORT THE FILES THAT GOING TO BE TESTED
# get all the tests names
test_file_strings = glob.glob("*Tests.py")
# clean extensions
module_strings = [str[0:len(str) - 3] for str in test_file_strings]

del module_strings[0]

# Go over the tests names

tests_to_run = []

for module in module_strings:
    i = importlib.import_module(module)
    # if module != 'Say_Tests':
    #     continue

    class_name = inspect.getmembers(i, inspect.isclass)

    if class_name[0][0] is not 'Postgres':
        get_class_name = class_name[0][0]
    else:
        get_class_name = class_name[1][0]

    tests_to_run.append(TestLoader().loadTestsFromTestCase(getattr(i, get_class_name)))

#tests_to_run = tests_to_run[42]

suite = TestSuite(tests_to_run)

runner = HTMLTestRunner(output='html_report', combine_reports=True, report_name="APIReport", report_title="API Tests Results")

#for x in range(2):
runner.run(suite)
