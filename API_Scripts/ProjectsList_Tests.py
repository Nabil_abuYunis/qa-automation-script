import unittest
import Renderer_Functions
from Database import Postgres
import random
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBprojectsListSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}
    # Read config file
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)


    def test_positive_1_projectsList(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]
        email = self.config.get('LOGINADMIN', 'email')

        acc = Renderer_Functions.get_allowed_accounts(email)
        proj = Renderer_Functions.get_allowed_projects_in_allowed_accounts(email)


        # Get account id
        permitted_accounts = Renderer_Functions.get_allowed_accounts(email)
        random_account = random.choice(permitted_accounts)
        # resp = Renderer_Functions.send_request(cookie, user, "accountlist")
        # # Get account that have projects within from sql instead of API
        cursor = Postgres.connect_to_db()
        # query = "SELECT distinct(account_id) FROM user_account_permissions where user_email like 'admin@speechmorphing.com' " \
        #         + "or user_email like '" + email + "'" + " in (select distinct account_id from project) " \
        #         + "intersect (select distinct account_id from project)"
        # cursor.execute(query)
        # db_info = [r for r in cursor]
        # # random_line = randint(0, resp["count"] - 1)
        # # account_id = resp["results"][random_line]["account_id"]
       # random_account = 'C2199023255560'

        # Get project list within the an account id that we got from above lines
        # user = {"account_id": random_account[0]}
        # user = {"account_id": 'C4398046511125'}
        user = {"account_id": random_account[0]}
        resp = Renderer_Functions.send_request(cookie, user, "projectsList")

        # Get the email-account permission_id first
        query = "SELECT user_permission_id FROM user_account_permissions WHERE account_id ILIKE '"+random_account[0] +"' AND user_email ILIKE '"+email+"'"
        cursor.execute(query)
        permission_id = [r for r in cursor]

        # Get the projects for the specified account and the user permission level
        query = "SELECT project_id ,project_name FROM project WHERE account_id ='"+ random_account[0] +"'"
         #   "AND project_id IN (SELECT project_id FROM user_project_permissions WHERE user_permission_id = "+str(permission_id[0][0])+")"
        # query = "SELECT project_id ,project_name FROM project WHERE account_id ='"+ random_account[0] +"' " \
        #      "AND project_id IN (SELECT project_id FROM user_project_permissions WHERE user_permission_id = "+str(permission_id[0][0])+\
        #      " and project_name not ilike '%Management%'"+")"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])
        resp["results"]=sorted(db_info, key=lambda k: k[0])
        # here we will check if the request is passed or failed
        count = resp["results"].__len__()

        # If they are not the same amount then fail
        if db_info.__len__() != count:
            assert False
        if (count >= 1):
         for x in range(count-1):
            if (self.assertEqual(resp["status_code"], 200)) is None \
                    and (self.assertEqual(resp["results"][x]["project_name"], db_info[x][1]) is None) \
                    and (self.assertEqual(resp["results"][x]["project_id"], db_info[x][0]) is None) :
                print ("Project List Pass")
                print("resp: {0}".format(resp))
                print("account_name: {0}".format(resp["results"][x]["project_name"]))
            else:
                print("Positive Test Failed")
                assert False

    print("Account List Pass")

    def test_negative_2_projectsList_wrong_login(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("123456789", user, "accountlist")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_projectsList_missing_login(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "accountlist")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_projectsList_expired_token(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "accountlist")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
