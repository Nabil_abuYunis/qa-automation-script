import urllib.error as urllibe
import unittest
import Renderer_Functions

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBLexiconSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_lexicon(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {"lexicon": "hello", "load": "load"}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "lexicon")

        if (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["success_message"], "success") is None):
            print("Get Voice Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))

    def test_negative_2_incorrect_lexicon(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        user = {"lexicon": 123456, "load": "load"}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "lexicon")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for lexicon.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_3_incorrect_load(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        user = {"lexicon": "hello", "load": 123456}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "lexicon")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for load.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_4_missing_lexicon(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        user = {"load": "load"}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "lexicon")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter lexicon.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_5_missing_load(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        user = {"lexicon": "hello"}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "lexicon")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter load.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_6_lexicon_wrong_login(self):
        # parameters
        user = {"lexicon": "hello", "load": "load"}
        resp = Renderer_Functions.send_request("123456789", user, "lexicon")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_7_lexicon_missing_login(self):
        # parameters
        user = {"lexicon": "hello", "load": "load"}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "lexicon")
        print(resp["status_code"])
        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_8_expired_token(self):
        # Parameters
        user = {"lexicon": "hello", "load": "load"}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "lexicon")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

                            
if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)