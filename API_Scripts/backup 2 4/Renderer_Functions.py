import urllib.request as urllibr
import urllib.error
import json
import requests
from socket import timeout
import re
import configparser
import logging
import SMI_API as SMI_API
import urllib.error as urllibe

# baseUrl = "http://50.197.139.145:8280"
# baseUrl = "http://ec2-52-53-250-125.us-west-1.compute.amazonaws.com:8080/"
# baseUrl = "http://10.1.10.131:8080"
# dbUrl = "10.1.10.131"
# baseUrl = "http://10.1.10.34:8080"
# dbUrl = "10.1.10.34"
#baseUrl = "http://smorph-demo-1608781781.us-east-1.elb.amazonaws.com"
#dbUrl = "smorph-demo-gpu.clx3qujijubm.us-east-1.rds.amazonaws.com"
# baseUrl = "ec2-54-67-28-43.us-west-1.compute.amazonaws.com"
# baseUrl = "http://smorph-demo-1608781781.us-east-1.elb.amazonaws.com/RenderingApp/"
# baseUrl = "https://smorph-say.speechmorphing.com:8443"
# baseUrl = "http://smorph-dev.speechmorphing.com:9090" #feedback
boundary = ""

config = configparser.ConfigParser()
config.sections()
config.read('api_config.ini')


def login(user):
    user_json = json.dumps(user).encode("utf-8")

    # create request
    print(config["SERVER"]["url"])
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "login")
    request = urllibr.Request(url)

    # add header to request
    headers = {"Content-Type": "application/json"}
    for key, value in headers.items():
        request.add_header(key, value)

    # t = api_request(user_json, request, url, headers)

    # post json data
    response = requests.post(url, user_json, headers)
    if response is None:
        print("*****No response from API*****")
    else:
        # print(response.content.decode("utf-8","strict"))
        # content_json = json.loads(response.content.decode("utf-8","strict"))
        content_json = response.json()
        print ("response.status_code {0}".format(response.status_code))

    print(response.status_code)

    if response.status_code == 200:
        try:
            resp = {}
            resp["status_code"] = response.status_code
            resp["content"] = content_json
            # print(content_json)
            resp["cookies"] = response.cookies.values()
            return resp
        except:
            resp = {}
            resp["status_code"] = response.status_code
            resp["error_code"] = content_json["results"]["error code"]
            resp["details"] = content_json["results"]["error message"]
            return resp
    else:
        resp = {}
        resp["status_code"] = response.status_code
        # print(content_json["results"])
        resp["details"] = content_json["results"]["error message"]
        return resp

        '''
        if response.status_code == 200:
            resp = {}
            resp["status_code"] = response.status_code
            resp["content"] = content_json
            resp["cookies"] = response.cookies.values()
            return resp

        else:
            resp = {}
            resp["status_code"] = response.status_code
            resp["error_code"] = content_json["Results"]["Error Code"]
            resp["details"] = content_json["Results"]["Error Message"]
            return resp
        '''

def initiate_login():

    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    # Get valid username and password
    email = config["LOGIN"]["email"]
    password = config["LOGIN"]["password"]
    user = {"email": email, "password": password}
    resp = login(user)

    if resp["status_code"] == 200:
        try:
            email_address = resp["content"]["results"]["user"]["email_address"]
            assert resp["status_code"] == 200
            assert email_address == email
            # if (self.assertEqual(resp["status_code"], 200)) is None \
            #         and (self.assertEqual(email_address, email) is None):
            print("Login Pass")
            print("Token: {0}".format(resp["cookies"]))
            return resp

        except urllibe.URLError as e:  # if response code is 200 but the fetched email is
            # different than the requested
            print("-|-Result:Test case Failed")
            print("Reason: {0}".format(e.reason))

    # Check error type
    elif resp["status_code"] != 200:
        print("Login Failed")
        print("Status Code: {0}".format(resp["status_code"]))
        print("Reason: {0}".format(resp["details"]))

def signup(user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "signup")
    request = urllibr.Request(url)
    
    # add header to requests
    headers = {"Content-Type": "application/json"}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def send_request(token, user, service):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], service)
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def svp(token):
    user_json = {}

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "svp")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)

    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def api_request(user_json, request, url, headers):
    # logger = init_logger()
    try:
        print("Getting response...")
        response = urllibr.urlopen(request, user_json, timeout=30)
        print("*******************")
        geronimo = response.read().decode("utf-8",'strict')
        print(geronimo)
        print("*******************")
        try:
            resp = json.loads(geronimo)
        except:
            print("Parsing Error: JSON failed to load")
            # logger.error("Parsing Error: JSON failed to load")
        #resp = null
        print("*******************")
        # this below "try" code is only for signup tests
        try:
            if resp["status code"] == 400:
                return resp
            
        except:
                
            resp["status_code"] = response.code
            #print('HTTPError: {}'.format(response.code))
            return resp

        
    except requests.exceptions.ConnectionError as disconnect:
        # logger.error ('Server Down', disconnect)
        print(('Server Down', disconnect))
        resp = None
        return resp

    except (urllib.error.HTTPError, urllib.error.URLError) as error:
        # logger.error ('Data not retrieved because of: %s', error)
        print('Data not retrieved because of: %s', error)
        resp = {}
        code = str(error).split(" ")
        regex = re.compile('[^0-9]')
        print(code)
        sc = regex.sub('', code[len(code)- 2])
        resp["status_code"] = sc
        resp["details"] = 'Unauthorized Access'
        return resp

    except timeout:
        # logger.error ('socket timed out - URL')
        print ('socket timed out - URL')
        resp = None
        return resp

    except:  # if the answer above is different than 200 then use the second method in order to get the reason
        response = requests.post(url, user_json, headers)
        print(response)
        resp = {}
        resp["status_code"] = response.status_code
        print("Response Code: " + str(resp["status_code"]))
        if response.status_code != 404:
            content_json = json.loads(response.content)
            
            try:
                resp["details"] = content_json["results"]["error_message"]
                # logger.error (resp["details"])
                print(resp["details"])
            except:  # the tests that uses sendUnsupported have the error in differnt path
                resp["details"] = content_json["Error"]
                # logger.error(resp["details"])
                print(resp["details"])
        return resp

def compare_two_values_equal(side1, side2):
    # Check first that side 2 with is from the DB is not None, if it is then replace it with 0

    side2 = '0' if side2 == 'None' else str(side2)

    # Compare
    if str(side1) == str(side2):
        return True
    else:
        return False

def init_logger(file_name):
    # logger = logging.getLogger(name)
    # logger.setLevel(logging.DEBUG)
    #
    # logs_folder = 'logs'
    # if not os.path.isdir(path):
    #     os.makedirs(path)
    #
    # logging_name = 'logs/results.log'
    #
    # # create file handler which logs debug messages
    # fh = logging.FileHandler(logging_name)
    # fh.setLevel(logging.DEBUG)
    #
    # # create console handler with a higher log level
    # ch = logging.StreamHandler()
    # ch.setLevel(logging.DEBUG)
    #
    # # create formatter and add it to the handlers
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # fh.setFormatter(formatter)
    # ch.setFormatter(formatter)
    #
    # # add the handlers to the logger
    # logger.addHandler(fh)
    # logger.addHandler(ch)
    #
    # return logger
    # create logger for logging purposes

    lgr = logging.getLogger(file_name)
    lgr.setLevel(logging.DEBUG)
    # add a file handler
    fh = logging.FileHandler('logs\\' + file_name)
    fh.setLevel(logging.DEBUG)
    # create a formatter and set the formatter for the handler.
    frmt = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(frmt)
    # add the Handler to the logger
    lgr.addHandler(fh)
    return lgr
