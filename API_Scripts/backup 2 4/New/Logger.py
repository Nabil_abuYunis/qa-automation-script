import logging
import os
import datetime as dt
import time

def init_logger(name, path):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    
    logs_folder = 'logs'
    if not os.path.isdir(path):
        os.makedirs(path)

    logging_name = 'logs/results.log'

    # create file handler which logs debug messages 
    fh = logging.FileHandler(logging_name)
    fh.setLevel(logging.DEBUG)
        
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
        
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
        
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)

    return logger

def get_current_date_string():
    '''
    Return a string for current date information
    '''
    current_date_object = dt.date.today()
    current_date_str = str(current_date_object.year) + \
                           str(current_date_object.month) \
                           + str(current_date_object.day) + '_' + str(int(time.time()))
            
    return current_date_str

