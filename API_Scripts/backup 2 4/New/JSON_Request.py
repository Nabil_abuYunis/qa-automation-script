import random
from random import randrange
import os
import json

def get_rand_ssml_value(path):
    ssml_file = path + "\\" + random.choice(os.listdir(path))
    return get_ssml_value(ssml_file)
    
def get_ssml_value(file):
    with open(file, encoding='utf-8') as ssml:
        text = ssml.read()
        domain = text.split('\n')
        stuff = ""
        for a in domain:
            stuff += a
        return stuff

def get_element_id(text_ssml, element):
    arr = text_ssml.split(' ')
    for a in arr:
        if element in a:
            b = a.split("\"")
            return(b[1])



def get_line_of_text(path):
    flag = True

    while flag:
        with open(path, encoding="utf8") as f:
            line = next(f)
            for num, aline in enumerate(f):
                if randrange(num + 2): continue
                line = aline

            if not line in ['\n', '\r\n'] and line != "":
                flag = False
                return line     

def clean_word(word):
    word = re.sub("&", "&amp;", word)
    word = re.sub("<", "&lt;", word)
    word = re.sub(">", "&gt;", word)
    word = re.sub("\"", "&quot;", word)
    word = re.sub("'", "&apos;", word)
    print("Galactic: " + word)
    return word.strip('\n')

def is_json(text):
    try:
        json_obj = json.loads(text)
    except ValueError as e:
        return False
    return True

def set_voice_request_data(send_unsupported,*args):
    keys = ['language_id','age_id','gender_id',
            'voicename','keyword','keyword_length']
    user = {"send_unsupported":send_unsupported}
    if len(args) > 0:
        num = 0
        for x in args:
            user[keys[num]] = str(x)
            num += 1
    return user

def set_domain_request_data(language_id,*args):
    keys = ['keyword','keyword_length','open_to_public',"send_unsupported"]
    user = {"language_id": language_id}
    if len(args) > 0:
        num = 0
        for x in args:
            user[keys[num]] = str(x)
            num += 1
    return user

def set_say_request_data(text_ssml,smi_voice_id,request_id,domain_id,*args):
    keys = ['url_type','file_format']
    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": domain_id,
                                "text_ssml": text_ssml}
    if len(args) > 0:
        num = 0
        for x in args:
            user[keys[num]] = str(x)
            num += 1
    return user

def set_template_request_data(text_ssml,smi_voice_id,request_id,domain_id,
                                  template_id, template_original, *args):
    keys = ['url_type','file_format']
    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": domain_id,
                "text_ssml": text_ssml, "template_id": template_id, "template_original": template_original}

    if len(args) > 0:
        num = 0
        for x in args:
            user[keys[num]] = str(x)
            num += 1
    return user

