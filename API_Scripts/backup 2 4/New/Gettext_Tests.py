import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint
import configparser
from Database import Postgres

class VBGettextSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    def test_positive_1_getext(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    # parameters
                    # filename id
                    # filename_id = 72057594037927937
                    # fetch filename id from filename functions
                    template = "no"
                    user = {"template": template}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    filename_id = resp_extract[random_line]["filename_id"]
                    print(filename_id)

                    # parameters
                    user = {"filename_id": filename_id}
                    resp = Renderer_Functions.gettext(resp["cookies"][0], user)
                    count = resp['count']
                    # Fetch the info from Database
                    cursor = Postgres.connect_to_db()
                    cursor.execute("SELECT  renderer_text_id, name, text_ssml,open_to_public,template_id, is_template_org, text_owner_email, smi_domain_id  FROM renderer_text where renderer_text_id = " + filename_id + "")

                    db_info = [r for r in cursor]
                    # db_info = sorted(db_info, key=lambda k: k[0])
                    print(db_info)
                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    # resp = sorted(resp["results"], key=lambda k: k['Text_Id'])
                    resp = resp["results"]

                    # Choose a random row to test
                    random_line = randint(0, count - 1)

                    is_temp_org = ""
                    if db_info[0][5] == True:
                        is_temp_org = "yes"
                    else:
                        is_temp_org = 'NULL'

                    temp_id = ""
                    print("%%%%")
                    print(db_info[0][4])
                    if db_info[0][4] is None:
                        temp_id = str(0)
                    else:
                        temp_id = str(db_info[0][4])

                    perm = ""
                    if db_info[0][3] == True:
                        perm = 'true'
                    else:
                        perm = 'false'

                    # here we will check if the voice detail is passed or failed
                    if (self.assertEqual(resp_status_code, 200)) is None \
                            and (self.assertEqual(resp["text_id"], str(db_info[0][0]))) is None \
                            and (self.assertEqual(resp["text_ssml"],db_info[0][2])) is None \
                            and (self.assertEqual(resp["name"], db_info[0][1])) is None \
                            and (self.assertEqual(resp["template_id"], temp_id)) is None \
                            and (self.assertEqual(resp["permission"], perm)) is None \
                            and (self.assertEqual(resp["template_original"], is_temp_org)) is None:
                        print("Get Voice Pass")
                        print("resp: {0}".format(resp))
                        print("status_code: {0}".format(resp_status_code))
                        print("Text_Id: {0}".format(resp["text_id"]))
                        print("Text_Ssml: {0}".format(resp["text_ssml"]))
                        print("Name: {0}".format(resp["name"]))
                        print("Permission: {0}".format(resp["permission"]))

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_2_gettext_wrong_login(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    # parameters
                    # filename id
                    # filename_id = 72057594037927937
                    # fetch filename id from filename functions
                    template = "NO"
                    user = {"normalization_domain_id": 1, "template": template}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    print("###")
                    print(resp2)
                    resp_extract = resp2["results"][0]['list']
                    filename_id = resp_extract[0]["filename_id"]

                    # parameters
                    user = {"filename_id": filename_id}
                    resp = Renderer_Functions.gettext("123456789", user)
                    # resp_results = resp["results"]

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], '401')) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp["details"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] == 200:
                        print("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_3_gettext_missing_login(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    # parameters
                    # filename id
                    # filename_id = 72057594037927937
                    # fetch filename id from filename functions
                    template = "NO"
                    user = {"normalization_domain_id": 1, "template": template}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    filename_id = resp_extract[0]["filename_id"]

                    # parameters
                    user = {"filename_id": filename_id}
                    resp = Renderer_Functions.gettext("NO TOKEN", user)
                    # resp_results = resp["results"]

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], '401')) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp["details"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] == 200:
                        print("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_4_gettext_incorrect_value(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    # parameters
                    user = {"filename_id": "chef boyardee"}
                    resp = Renderer_Functions.gettext(resp["cookies"][0], user)
                    # resp_results = resp["results"]

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error_message"],
                                                          "Incorrect value for filename_id.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error_message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))


    def test_negative_5_gettext_missing_param(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    # parameters
                    user = {}
                    resp = Renderer_Functions.gettext(resp["cookies"][0], user)
                    # resp_results = resp["results"]

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error_message"],
                                                          "Missing Parameter filename_id.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error_message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"filename_id": "765489612"}
        resp = Renderer_Functions.gettext("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
