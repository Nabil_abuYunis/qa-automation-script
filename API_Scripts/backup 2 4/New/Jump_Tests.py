import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint
import configparser
from Database import Postgres
import JSON_Request as Data_Utils

class VBJumpSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    def test_positive_1_jump(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                            'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                            'is open on a holiday?</speak>'
                smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                request_id = "654334"
                #request_id = "123456"
                smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                #file_format = "wav"

                user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                        "text_ssml": text_ssml}
                token = resp["cookies"][0]
                resp = Renderer_Functions.say(token, user)

                #Parameters
                filenumber = resp['results']['filenumber']
                direction = "forward"
                units = "words"
                length_of_jump = 10

                user = {"filenumber": filenumber, "direction": direction, "units": units,
                        "length_of_jump": length_of_jump}
                resp = Renderer_Functions.jump(token, user)

                if (self.assertEqual(resp["status_code"], 200)) is None \
                   and (self.assertEqual(resp['results']['success_message'], "success")) is None:
                     print("Jump Pass")
                     print("resp: {0}".format(resp))

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_2_missing_filenumber(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("Login Pass")
                    print ("Token: {0}".format(resp["cookies"]))

                #Parameters
                #filenumber = resp['results']['filenumber']
                direction = "forward"
                units = "words"
                length_of_jump = 10

                user = {"direction": direction, "units": units,
                        "length_of_jump": length_of_jump}
                resp = Renderer_Functions.jump(resp["cookies"][0], user)

                # here we will check if the request is passed or failed
                if resp["status_code"] == 200:
                    try:
                        if (self.assertEqual(resp["status_code"], 200)) is None \
                                and (self.assertEqual(resp["results"]["error_message"],
                                                      "Missing Parameter filenumber.") is None):
                            print ("-|-Result:Test case Pass")
                            print ("resp: {0}".format(resp))
                            print ("Status Code: {0}".format(resp["status_code"]))
                            print ("Reason: {0}".format(resp["results"]["error_message"]))

                    except urllibe.URLError as e:
                        print ("-|-Result:Test case Failed")
                        print ("Reason: {0}".format(e.reason))

                # Check error type
                elif resp["status_code"] != 200:
                    print ("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Login Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))

    def test_negative_3_missing_direction(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("Login Pass")
                    print ("Token: {0}".format(resp["cookies"]))

                text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                            'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                            'is open on a holiday?</speak>'
                smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                request_id = "654334"
                #request_id = "123456"
                smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                #file_format = "wav"

                user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                        "text_ssml": text_ssml}
                token = resp["cookies"][0]
                resp = Renderer_Functions.say(token, user)

                #Parameters
                filenumber = resp['results']['filenumber']
                direction = "forward"
                units = "words"
                length_of_jump = 10

                user = {"filenumber": filenumber, "units": units,
                        "length_of_jump": length_of_jump}
                resp = Renderer_Functions.jump(token, user)

                # here we will check if the request is passed or failed
                if resp["status_code"] == 200:
                    try:
                        if (self.assertEqual(resp["status_code"], 200)) is None \
                                and (self.assertEqual(resp["results"]["error_message"],
                                                      "Missing Parameter direction.") is None):
                            print ("-|-Result:Test case Pass")
                            print ("resp: {0}".format(resp))
                            print ("Status Code: {0}".format(resp["status_code"]))
                            print ("Reason: {0}".format(resp["results"]["error_message"]))

                    except urllibe.URLError as e:
                        print ("-|-Result:Test case Failed")
                        print ("Reason: {0}".format(e.reason))

                # Check error type
                elif resp["status_code"] != 200:
                    print ("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Login Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))

    def test_negative_4_missing_units(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("Login Pass")
                    print ("Token: {0}".format(resp["cookies"]))

                text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                            'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                            'is open on a holiday?</speak>'
                smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                request_id = "654334"
                #request_id = "123456"
                smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                #file_format = "wav"

                user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                        "text_ssml": text_ssml}
                token = resp["cookies"][0]
                resp = Renderer_Functions.say(token, user)
                #Parameters
                filenumber = resp['results']['filenumber']
                direction = "forward"
                units = "words"
                length_of_jump = 10

                user = {"filenumber": filenumber, "direction": direction,
                        "length_of_jump": length_of_jump}
                resp = Renderer_Functions.jump(token, user)

                # here we will check if the request is passed or failed
                if resp["status_code"] == 200:
                    try:
                        if (self.assertEqual(resp["status_code"], 200)) is None \
                                and (self.assertEqual(resp["results"]["error_message"],
                                                      "Missing Parameter units.") is None):
                            print ("-|-Result:Test case Pass")
                            print ("resp: {0}".format(resp))
                            print ("Status Code: {0}".format(resp["status_code"]))
                            print ("Reason: {0}".format(resp["results"]["error_message"]))

                    except urllibe.URLError as e:
                        print ("-|-Result:Test case Failed")
                        print ("Reason: {0}".format(e.reason))

                # Check error type
                elif resp["status_code"] != 200:
                    print ("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Login Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))

    def test_negative_5_missing_length_of_jump(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("Login Pass")
                    print ("Token: {0}".format(resp["cookies"]))

                text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                            'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                            'is open on a holiday?</speak>'
                smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                request_id = "654334"
                #request_id = "123456"
                smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                #file_format = "wav"

                user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                        "text_ssml": text_ssml}
                token = resp["cookies"][0]
                resp = Renderer_Functions.say(token, user)
                #Parameters
                filenumber = resp['results']['filenumber']
                direction = "forward"
                units = "words"
                length_of_jump = 10

                user = {"filenumber": filenumber, "direction": direction,
                        "units": units}
                resp = Renderer_Functions.jump(token, user)

                # here we will check if the request is passed or failed
                if resp["status_code"] == 200:
                    try:
                        if (self.assertEqual(resp["status_code"], 200)) is None \
                                and (self.assertEqual(resp["results"]["error_message"],
                                                      "Missing Parameter length_of_jump.") is None):
                            print ("-|-Result:Test case Pass")
                            print ("resp: {0}".format(resp))
                            print ("Status Code: {0}".format(resp["status_code"]))
                            print ("Reason: {0}".format(resp["results"]["error_message"]))

                    except urllibe.URLError as e:
                        print ("-|-Result:Test case Failed")
                        print ("Reason: {0}".format(e.reason))

                # Check error type
                elif resp["status_code"] != 200:
                    print ("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Login Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))

    def test_negative_6_incorrect_filenumber(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("Login Pass")
                    print ("Token: {0}".format(resp["cookies"]))

                #Parameters
                filenumber = "pzsjdfkisa"
                direction = "forward"
                units = "words"
                length_of_jump = 10

                user = {"filenumber": filenumber, "direction": direction, "units": units,
                        "length_of_jump": length_of_jump}
                resp = Renderer_Functions.jump(resp["cookies"][0], user)

                # here we will check if the request is passed or failed
                if resp["status_code"] == 200:
                    try:
                        if (self.assertEqual(resp["status_code"], 200)) is None \
                                and (self.assertEqual(resp["results"]["error_message"],
                                                      "Incorrect value for filenumber.") is None):
                            print ("-|-Result:Test case Pass")
                            print ("resp: {0}".format(resp))
                            print ("Status Code: {0}".format(resp["status_code"]))
                            print ("Reason: {0}".format(resp["results"]["error_message"]))

                    except urllibe.URLError as e:
                        print ("-|-Result:Test case Failed")
                        print ("Reason: {0}".format(e.reason))

                # Check error type
                elif resp["status_code"] != 200:
                    print ("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Login Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))

    def test_negative_7_incorrect_direction(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("Login Pass")
                    print ("Token: {0}".format(resp["cookies"]))

                text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                            'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                            'is open on a holiday?</speak>'
                smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                request_id = "654334"
                #request_id = "123456"
                smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                #file_format = "wav"

                user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                        "text_ssml": text_ssml}
                token = resp["cookies"][0]
                resp = Renderer_Functions.say(token, user)
                #Parameters
                filenumber = resp['results']['filenumber']
                direction = 123456789
                units = "words"
                length_of_jump = 10

                user = {"filenumber": filenumber, "direction": direction, "units": units,
                        "units": units, "length_of_jump": length_of_jump}
                resp = Renderer_Functions.jump(token, user)

                # here we will check if the request is passed or failed
                if resp["status_code"] == 200:
                    try:
                        if (self.assertEqual(resp["status_code"], 200)) is None \
                                and (self.assertEqual(resp["results"]["error_message"],
                                                      "Incorrect value for direction.") is None):
                            print ("-|-Result:Test case Pass")
                            print ("resp: {0}".format(resp))
                            print ("Status Code: {0}".format(resp["status_code"]))
                            print ("Reason: {0}".format(resp["results"]["error_message"]))

                    except urllibe.URLError as e:
                        print ("-|-Result:Test case Failed")
                        print ("Reason: {0}".format(e.reason))

                # Check error type
                elif resp["status_code"] != 200:
                    print ("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Login Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))

    def test_negative_8_incorrect_units(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("Login Pass")
                    print ("Token: {0}".format(resp["cookies"]))

                text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                            'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                            'is open on a holiday?</speak>'
                smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                request_id = "654334"
                #request_id = "123456"
                smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                #file_format = "wav"

                user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                        "text_ssml": text_ssml}
                token = resp["cookies"][0]
                resp = Renderer_Functions.say(token, user)
                #Parameters
                filenumber = resp['results']['filenumber']
                direction = "forward"
                units = 123456789
                length_of_jump = 10

                user = {"filenumber": filenumber, "direction": direction, "units": units,
                        "units": units,"length_of_jump":length_of_jump}
                resp = Renderer_Functions.jump(token, user)

                # here we will check if the request is passed or failed
                if resp["status_code"] == 200:
                    try:
                        if (self.assertEqual(resp["status_code"], 200)) is None \
                                and (self.assertEqual(resp["results"]["error_message"],
                                                      "Incorrect value for units.") is None):
                            print ("-|-Result:Test case Pass")
                            print ("resp: {0}".format(resp))
                            print ("Status Code: {0}".format(resp["status_code"]))
                            print ("Reason: {0}".format(resp["results"]["error_message"]))

                    except urllibe.URLError as e:
                        print ("-|-Result:Test case Failed")
                        print ("Reason: {0}".format(e.reason))

                # Check error type
                elif resp["status_code"] != 200:
                    print ("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Login Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))

    def test_negative_9_incorrect_length_of_jump(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("Login Pass")
                    print ("Token: {0}".format(resp["cookies"]))

                text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                            'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                            'is open on a holiday?</speak>'
                smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                request_id = "654334"
                #request_id = "123456"
                smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                #file_format = "wav"

                user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                        "text_ssml": text_ssml}
                token = resp["cookies"][0]
                resp = Renderer_Functions.say(token, user)
                #Parameters
                filenumber = resp['results']['filenumber']
                direction = "forward"
                units = "words"
                length_of_jump = "nbnfbidsask"

                user = {"filenumber": filenumber, "direction": direction, "units": units,
                        "units": units,"length_of_jump":length_of_jump}
                resp = Renderer_Functions.jump(token, user)

                # here we will check if the request is passed or failed
                if resp["status_code"] == 200:
                    try:
                        if (self.assertEqual(resp["status_code"], 200)) is None \
                                and (self.assertEqual(resp["results"]["error_message"],
                                                      "Incorrect value for length_of_jump.") is None):
                            print ("-|-Result:Test case Pass")
                            print ("resp: {0}".format(resp))
                            print ("Status Code: {0}".format(resp["status_code"]))
                            print ("Reason: {0}".format(resp["results"]["error_message"]))

                    except urllibe.URLError as e:
                        print ("-|-Result:Test case Failed")
                        print ("Reason: {0}".format(e.reason))

                # Check error type
                elif resp["status_code"] != 200:
                    print ("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Login Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))

    def test_negative_10_wrong_login(self):
        # Parameters
        filenumber = "71234569"
        direction = "forward"
        units = "words"
        length_of_jump = 10

        user = {"filenumber": filenumber, "direction": direction, "units": units,
                "units": units}
        resp = Renderer_Functions.jump("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_11_missing_login(self):
        # Parameters
        filenumber = "71234569"
        direction = "forward"
        units = "words"
        length_of_jump = 10

        user = {"filenumber": filenumber, "direction": direction, "units": units,
                "units": units}
        resp = Renderer_Functions.jump("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_12_expired_token(self):
        # Parameters
        filenumber = "71234569"
        direction = "forward"
        units = "words"
        length_of_jump = 10

        user = {"filenumber": filenumber, "direction": direction, "units": units,
                "units": units}
        resp = Renderer_Functions.jump("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")
                    

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
