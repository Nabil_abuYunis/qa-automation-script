import urllib2
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint

class VBVoiceDetailsSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Start run test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Completed running test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"
        print self.currentResult

    def test_positive_1_voice_details(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    smi_voice_id = "72057594037927938"
                    user = {"smi_voice_id": smi_voice_id}
                    resp = Renderer_Functions.voice_details(resp["cookies"][0], user)
                    resp_results = resp["Results"][0]

                    # Fetch the info from Database
                    cursor = Renderer_Functions.connect_to_db()
                    cursor.execute("SELECT name, gender_id, age_id, language_id, img_url FROM smi_voice  where smi_voice_id = " + smi_voice_id)

                    db_info = [r for r in cursor]
                    db_info = sorted(db_info, key=lambda k: k[0])

                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    # resp = sorted(resp["Results"], key=lambda k: k['Mood_Id'])

                    # Choose a random row to test
                    random_line = randint(0, len(db_info) - 1)

                    Img_Url_name_only = resp_results["img_url"].split('/')

                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp_status_code, 200)) is None \
                            and (self.assertEqual(resp_results["gender_id"], db_info[random_line][1])) is None \
                            and (self.assertEqual(resp_results["age_id"], db_info[random_line][2])) is None \
                            and (self.assertEqual(Img_Url_name_only[4], db_info[random_line][4])) is None \
                            and (self.assertEqual(resp_results["name"], db_info[random_line][0])) is None \
                            and (self.assertEqual(resp_results["language_id"], db_info[random_line][3])) is None:
                        print "Voice Details Pass"
                        print "resp: {0}".format(resp)
                        print "status_code: {0}".format(resp_status_code)
                        print "gender_id: {0}".format(resp_results["gender_id"])
                        print "age_id: {0}".format(resp_results["age_id"])
                        print "img_url: {0}".format(resp_results["img_url"])
                        print "name: {0}".format(resp_results["name"])
                        print "language_id: {0}".format(resp_results["language_id"])

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_2_voice_details_wrong_login(self):
                    # Parameters
                    smi_voice_id = "72057594037927938"
                    user = {"smi_voice_id": smi_voice_id}
                    resp = Renderer_Functions.voice_details("123456789", user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

    def test_negative_3_voice_details_missing_login(self):
                    # Parameters
                    smi_voice_id = "72057594037927938"
                    user = {"smi_voice_id": smi_voice_id}
                    resp = Renderer_Functions.voice_details("NO TOKEN", user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

if __name__ == '__main__':
    print "-------------Test Result----------------\n"
    testResult = unittest.main(verbosity=1)
