
Purpose of the API scripts files
These scripts are created in order to test the API functionalities for the rendering applicaiton It is possible to run each script file alone, but in order to make the procces easier, we created a file "Run_All.py" in order to run all the tests together.

Run_All.py - run all the script files
Run_All.py - this file runs all the test cases according to three parameters: You may enter 3 parameters, if they are not entered then a default parameters are taken, these parameters could be also changed by editing this file. you could run this file both in "CMD" and "Pycharm" program.
1. test_type - in order to pick the positive, negative or all tests:                     
	Positive: test_positive                     
	Negative: test_negative                     
	All tests: test   
2. test_file - in order to choose which files to test:                     
	All tests: Tests.py                     
	Specific tests: for example - ge_Te*   
3. test_dir - the folder that contains the test scripts