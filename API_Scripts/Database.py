import pypyodbc
import psycopg2
#import MySQLdb as mdb
import psycopg2 as pgdb
import psycopg2.extras
import configparser
import redis
import Renderer_Functions

config = configparser.ConfigParser()
config.sections()
config.read('api_config.ini')
# logger = Loggit.init_logger('DATABASE',config['LOCATION']['logs'])
# logger = Renderer_Functions.init_logger()

class Redis():
    def __init__(self,url,port,pw):
        try:
            self.conn = redis.StrictRedis(
                host=url,
                port=int(port),
                password=pw)
            # logger.info (self.conn)
            self.conn.ping()
            # logger.debug ('Connected to Redis!')
        except Exception as ex:
            print (ex)
            exit('Failed to connect, terminating.')

    def get_latest_key(self,req_id):
        arr = []
        for user in self.conn.scan_iter(match='AccumulatedTimings*'):
            key = user.decode("utf-8")
            arr.append(key)

        arr.sort()
        csec = arr[len(arr)-1]
        return csec

    def get_request_id(self):
        req_id = self.conn.get("sayRequestId").decode("utf-8")
        request_id = int(req_id) + 1
        return str(request_id)

    def get_key_value(self,req_id):
        key_name = 'AccumulatedTimings:Request:'+req_id
        # logger.info("Redis Key: {0}".format(key_name))
        value = self.conn.get(key_name).decode("utf-8")
        return value

class Postgres():
    def connect_to_db():

        dbUrl = config['SERVER']['db']
        dbPass = config['SERVER']['password']
        '''
        connection = pyodbc.connect('Driver={SQL Server};'
                                      'SERVER=Ss-werver=smorphdb.crlxwxg8ztgz.uest-1.rds.amazonaws.com;'
                                      'port=5432;'
                                      'Database=voicebank_Says;'
                                      'uid=vbuser;pwd=gfdsa12345!!')
        '''
        '''
        connection = pypyodbc.connect(driver='{SQL Server}', server="Ss-werver=smorphdb.crlxwxg8ztgz.uest-1.rds.amazonaws.com", host="localhost", port="5432" , database="voicebank_Says",
                              trusted_connection="yes", user="vbuser", password="gfdsa12345!!")
        '''

        '''
        connection = pyodbc.connect('DRIVER={SQL Server};SERVER=Ss-werver=smorphdb.crlxwxg8ztgz.uest-1.rds.amazonaws.com;PORT:5432;DATABASE=voicebank_Says;UID=vbuser;PWD=gfdsa12345!!')
        '''

        '''
        connection_string = "host='Ss-werver=smorphdb.crlxwxg8ztgz.uest-1.rds.amazonaws.com' port='5432' dbname='voicebank_Says' user='vbuser' password='gfdsa12345!!'"
        connection = psycopg2.connect(connection_string)
        '''
        '''
        connection = psycopg2.connect(database="voicebank_Says", user="vbuser", password="gfdsa12345!!", host="host='Ss-werver=smorphdb.crlxwxg8ztgz.uest-1.rds.amazonaws.com", port="5432")
        print "Opened database successfully"

        print "connbefore {0}".format(connection)
        '''

        # # self.con = mdb.connect(config.DB_HOSTNAME,config.DB_USERNAME, config.DB_PASSWORD, config.DB_NAME)
        # connection = pgdb.connect("dbname='voicebank_Says' user='vbuser' host='Ss-werver=smorphdb.crlxwxg8ztgz.uest-1.rds.amazonaws.com' port='5432' password='gfdsa12345!!'")

        connection = pgdb.connect("dbname='voicebank' user='vbuser' host=" + dbUrl + " port='5432' password='"+dbPass+"'")
        cur = connection.cursor()

        return cur
    
        # closing connection

    def connect_to_db_dictionary():

        dbUrl = config['SERVER']['db']
        dbPass = config['SERVER']['password']
        connection = pgdb.connect("dbname='dictionary_manager_EN' user='vbuser' host=" + dbUrl + " port='5432' password='"+dbPass+"'")
        cur = connection.cursor()

        return cur


    def connect_to_db_configurationdb():
        dbUrl = config['SERVER']['db']
        dbPass = config['SERVER']['password']
        connection = pgdb.connect(
            "dbname='configurationDB' user='vbuser' host=" + dbUrl + " port='5432' password='" + dbPass + "'")
        cur = connection.cursor()

        return cur

class Monitor():

    def __init__(self,connection_pool):
        self.connection_pool = connection_pool
        self.connection = None

    def __del__(self):
        try:
            self.reset()
        except:
            pass

    def reset(self):
        if self.connection:
            self.connection_pool.release(self.connection)
            self.connection = None

    def monitor(self):
        if self.connection is None:
            self.connection = self.connection_pool.get_connection('monitor',None)
        self.connection.send_command('monitor')
        return self.listen()

    def parse_response(self):
        return self.connection.read_response()

    def listen(self):
        while True:
            yield self.parse_response()
'''
if __name__ == '__main__':
    pool = redis.ConnectionPool(host = '10.1.10.16', port=6379, db=0)
    monitor = Monitor(pool)
    commands = monitor.monitor()

    for c in commands:
        print(c)
'''
