import urllib.error as urllibe
import unittest
import Renderer_Functions
from Database import Postgres
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBWordReplacementSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_word_replacement(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        cursor = Postgres.connect_to_db()

        # Get random smi_domain_id
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])

        user = {"smi_domain_id": smi_domain_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "wordreplacement")
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k['word'])

        # Fetch replacements words from DB
        query = 'WITH RECURSIVE parents (c_id, p_id, level) as (SELECT smi_domain_id, ' \
        'parent_id, 0 FROM smi_domain WHERE smi_domain_id = ' + str(smi_domain_id) +  \
        ' and marked_for_deletion is NULL UNION ALL SELECT smi_domain_id, parent_id, level + 1 ' \
        'FROM smi_domain INNER JOIN parents ON (smi_domain_id = p_id)) SELECT smi_domain_id, ' \
        'domain_name, word, replacement_word, use_gesture, gesture_id, gesture_name FROM smi_domain ' \
        'INNER JOIN (SELECT smi_domain_id, word, replacement_word, use_gesture, gesture_id, level, ' \
        'rank() OVER (PARTITION BY word ORDER BY level), g.name gesture_name FROM ' \
        'smi_domain_disallowed_words INNER JOIN parents ON (smi_domain_id = c_id) LEFT OUTER JOIN ' \
        'gesture g USING (gesture_id)) innerQuery USING (smi_domain_id) WHERE rank = 1 AND ' \
        '((use_gesture AND gesture_name IS NOT NULL) OR  (use_gesture IS DISTINCT FROM TRUE ' \
        'AND gesture_name IS NULL)) ORDER BY 2, 3;'
        cursor.execute(query)
        replacements_DB = [r for r in cursor]
        replacements_DB = sorted(replacements_DB, key=lambda k: k[2])

        count = resp.__len__()
        for x in range(count):
            if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp[x]["domain_name"], replacements_DB[x][1]) is None) \
                and (self.assertEqual(resp[x]["smi_domain_id"], str(replacements_DB[x][0])) is None) \
                and (self.assertEqual(resp[x]["use_gesture"], replacements_DB[x][4]) is None) \
                and (self.assertEqual(resp[x]["replacement_word"], replacements_DB[x][3]) is None) \
                and (self.assertEqual(resp[x]["word"], replacements_DB[x][2]) is None):
                # and (self.assertEqual(resp['results'][x]["gesture_id"], "") is None) \
                print(resp[x]["domain_name"], replacements_DB[x][1])
                print(resp[x]["smi_domain_id"], replacements_DB[x][0])
                print(resp[x]["use_gesture"], replacements_DB[x][4])
                print(resp[x]["replacement_word"], replacements_DB[x][3])
                print(resp[x]["word"], replacements_DB[x][2])

        print("Word Replacement Pass")
        print("resp: {0}".format(resp))
        print("status_code: {0}".format(resp_status_code))

    def test_negative_2_incorrect_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        user = {"smi_domain_id": "mistletoe"}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "wordreplacement")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for smi_domain_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_3_missing_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "wordreplacement")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter smi_domain_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_4_word_replacement_wrong_login(self):
        # parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("123456789", user, "wordreplacement")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_5_word_replacement_missing_login(self):
        # parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "wordreplacement")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "wordreplacement")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")


if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)