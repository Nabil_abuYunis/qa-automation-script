import unittest
import Renderer_Functions
from Database import Postgres
import random
import configparser
import datetime

import string

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')


class VBTemplatesUpdateSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Read config file
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)

    def test_positive_1_updateTemplate(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']
        cursor = Postgres.connect_to_db()

        # Get random request data
        # Get template ids
        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE template_id IN (SELECT DISTINCT template_id FROM renderer_text where template_id in( SELECT  renderer_text.template_id FROM renderer_text GROUP BY template_id HAVING COUNT(renderer_text_id) < 2))"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0,db_info.__len__())
        template_id = db_info[index][0]
        domain_id = db_info[index][1]

        # Get text_ssml,template_name,open_to_public values
        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id="+str(template_id)+" AND smi_domain_id="+str(domain_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]
        is_public = db_info[0][2]

        # Get project_id and language_id values
        query_domain = "SELECT smi_domain_owner_project_id,language_id FROM smi_domain WHERE smi_domain_id="+str(domain_id)
        cursor.execute(query_domain)
        db_info = [r for r in cursor]
        project_id = db_info[0][0]
        language_id = db_info[0][1]

        # Get template words
        query_temp_words = "SELECT  * FROM template_words WHERE template_id="+str(template_id)
        cursor.execute(query_temp_words)
        db_info = [r for r in cursor]
        words = []
        for x in range(db_info.__len__()):
            template_phonemes = []
            query_temp_phonemes = "SELECT word_phoneme_number,phoneme_text,stress_id,tobi_pitch_accent FROM " \
                                  "template_words_phonemes WHERE template_id=" + str(template_id) + " AND word_number=" + str(db_info[x][1])
            cursor.execute(query_temp_phonemes)
            db_phonemes = [r for r in cursor]
            for y in range(db_phonemes.__len__()):
                template_phonemes.append({"phoneme_number": db_phonemes[y][0], "phoneme": db_phonemes[y][1], "stress_type": db_phonemes[y][2], "pitch_type": db_phonemes[y][3]})
            template_words = {
                "word_number": db_info[x][1],
                "sub_field_id": 0 if db_info[x][2] is None else db_info[x][2],
                "field_type_id": 0 if db_info[x][9] is None else db_info[x][9],
                "pos_code": db_info[x][4],
                "boundary_tone_id": db_info[x][5],
                "phrase_break_id": db_info[x][6],
                "focus": db_info[x][7],
                "compressed_f0": True if db_info[x][8] == 1 else False,
                "text": db_info[x][3],
                "sensitivity_next_word": True if db_info[x][12] == 1 else False,
                "sensitivity_gender": True if db_info[x][10] == 1 else False,
                "mood_id": 0 if db_info[x][11] is None else db_info[x][11],"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100",
                "phonemes": template_phonemes}
            words.append(template_words)

        # Set user parameters
        user = {"template_id": template_id, "domain_id": domain_id, "template_name": template_name, "is_public": is_public, "text_ssml": text_ssml
                , "language_id": language_id, "project_id": project_id, "words": words}

        # Send request
        resp = Renderer_Functions.send_request(cookie, user, "templateupdate")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp['status_code'], 200)) is None \
                and (self.assertEqual(resp["results"]["template_id"], template_id) is None):
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False
        print("Templates Update Pass")


    def test_negative_2_updateTemplate_missing_domainId(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']
        cursor = Postgres.connect_to_db()

        # Get random request data
        # Get template ids
        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE template_id IN (SELECT DISTINCT template_id FROM renderer_text where template_id in( SELECT  renderer_text.template_id FROM renderer_text GROUP BY template_id HAVING COUNT(renderer_text_id) < 2))"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0,db_info.__len__())
        template_id = db_info[index][0]
        domain_id = db_info[index][1]

        # Get text_ssml,template_name,open_to_public values
        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id="+str(template_id)+" AND smi_domain_id="+str(domain_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]
        is_public = db_info[0][2]

        # Get project_id and language_id values
        query_domain = "SELECT smi_domain_owner_project_id,language_id FROM smi_domain WHERE smi_domain_id="+str(domain_id)
        cursor.execute(query_domain)
        db_info = [r for r in cursor]
        project_id = db_info[0][0]
        language_id = db_info[0][1]

        # Get template words
        query_temp_words = "SELECT  * FROM template_words WHERE template_id="+str(template_id)
        cursor.execute(query_temp_words)
        db_info = [r for r in cursor]
        words = []

        for x in range(db_info.__len__()):
            template_phonemes = []
            query_temp_phonemes = "SELECT word_phoneme_number,phoneme_text,stress_id,tobi_pitch_accent FROM " \
                                  "template_words_phonemes WHERE template_id=" + str(template_id) + " AND word_number=" + str(db_info[x][1])
            cursor.execute(query_temp_phonemes)
            db_phonemes = [r for r in cursor]
            for y in range(db_phonemes.__len__()):
                template_phonemes.append({"phoneme_number": db_phonemes[y][0], "phoneme": db_phonemes[y][1], "stress_type": db_phonemes[y][2], "pitch_type": db_phonemes[y][3]})
            template_words = {
                "word_number": db_info[x][1],
                "sub_field_id": 0 if db_info[x][2] is None else db_info[x][2],
                "field_type_id": 0 if db_info[x][9] is None else db_info[x][9],
                "pos_code": db_info[x][4],
                "boundary_tone_id": db_info[x][5],
                "phrase_break_id": db_info[x][6],
                "focus": db_info[x][7],
                "compressed_f0": True if db_info[x][8] == 1 else False,
                "text": db_info[x][3],
                "sensitivity_next_word": True if db_info[x][12] == 1 else False,
                "sensitivity_gender": True if db_info[x][10] == 1 else False,
                "mood_id": 0 if db_info[x][11] is None else db_info[x][11],"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100",
                "phonemes": template_phonemes}
            words.append(template_words)

        # Set user parameters
        user = {"template_id": template_id, "template_name": template_name, "is_public": is_public, "text_ssml": text_ssml
                , "language_id": language_id, "project_id": project_id, "words": words}

        # Send request
        resp = Renderer_Functions.send_request(cookie, user, "templateupdate")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp['status_code'], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"], "missing parameter domain_id.") is None):
            print("resp: {0}".format(resp))
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_3_updateTemplate_incorrect_domainId(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']
        cursor = Postgres.connect_to_db()

        # Get random request data
        # Get template ids
        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE template_id IN (SELECT DISTINCT template_id FROM renderer_text where template_id in( SELECT  renderer_text.template_id FROM renderer_text GROUP BY template_id HAVING COUNT(renderer_text_id) < 2))"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0,db_info.__len__())
        template_id = db_info[index][0]
        domain_id = db_info[index][1]

        # Get text_ssml,template_name,open_to_public values
        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE template_id="+str(template_id)+" AND smi_domain_id="+str(domain_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]
        is_public = db_info[0][2]

        # Get project_id and language_id values
        query_domain = "SELECT smi_domain_owner_project_id,language_id FROM smi_domain WHERE smi_domain_id="+str(domain_id)
        cursor.execute(query_domain)
        db_info = [r for r in cursor]
        project_id = db_info[0][0]
        language_id = db_info[0][1]

        # Get template words
        query_temp_words = "SELECT  * FROM template_words WHERE template_id="+str(template_id)
        cursor.execute(query_temp_words)
        db_info = [r for r in cursor]
        words = []

        for x in range(db_info.__len__()):
            template_phonemes = []
            query_temp_phonemes = "SELECT word_phoneme_number,phoneme_text,stress_id,tobi_pitch_accent FROM " \
                                  "template_words_phonemes WHERE template_id=" + str(template_id) + " AND word_number=" + str(db_info[x][1])
            cursor.execute(query_temp_phonemes)
            db_phonemes = [r for r in cursor]
            for y in range(db_phonemes.__len__()):
                template_phonemes.append({"phoneme_number": db_phonemes[y][0], "phoneme": db_phonemes[y][1], "stress_type": db_phonemes[y][2], "pitch_type": db_phonemes[y][3]})
            template_words = {
                "word_number": db_info[x][1],
                "sub_field_id": 0 if db_info[x][2] is None else db_info[x][2],
                "field_type_id": 0 if db_info[x][9] is None else db_info[x][9],
                "pos_code": db_info[x][4],
                "boundary_tone_id": db_info[x][5],
                "phrase_break_id": db_info[x][6],
                "focus": db_info[x][7],
                "compressed_f0": True if db_info[x][8] == 1 else False,
                "text": db_info[x][3],
                "sensitivity_next_word": True if db_info[x][12] == 1 else False,
                "sensitivity_gender": True if db_info[x][10] == 1 else False,
                "mood_id": 0 if db_info[x][11] is None else db_info[x][11],"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100",
                "phonemes": template_phonemes}
            words.append(template_words)

        # Set user parameters
        domain_id = ''.join(random.choice(string.ascii_letters) for ii in range(6))
        user = {"template_id": template_id,"domain_id": domain_id, "template_name": template_name, "is_public": is_public, "text_ssml": text_ssml
                , "language_id": language_id, "project_id": project_id, "words": words}

        # Send request
        resp = Renderer_Functions.send_request(cookie, user, "templateupdate")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp['status_code'], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"], "incorrect value for domain_id.") is None):
            print("resp: {0}".format(resp))
        else:
            print("Negative Test Failed")
            assert False


    def test_negative_4_updateTemplate_incorrect_language_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']
        cursor = Postgres.connect_to_db()

        # Get random request data
        # Get template ids
        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE template_id IN (SELECT DISTINCT template_id FROM renderer_text where template_id in( SELECT  renderer_text.template_id FROM renderer_text GROUP BY template_id HAVING COUNT(renderer_text_id) < 2))"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0,db_info.__len__())
        template_id = db_info[index][0]
        domain_id = db_info[index][1]

        # Get text_ssml,template_name,open_to_public values
        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id="+str(template_id)+" AND smi_domain_id="+str(domain_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]
        is_public = db_info[0][2]

        # Get project_id and language_id values
        query_domain = "SELECT smi_domain_owner_project_id,language_id FROM smi_domain WHERE smi_domain_id="+str(domain_id)
        cursor.execute(query_domain)
        db_info = [r for r in cursor]
        project_id = db_info[0][0]
        language_id = db_info[0][1]

        # Get template words
        query_temp_words = "SELECT  * FROM template_words WHERE template_id="+str(template_id)
        cursor.execute(query_temp_words)
        db_info = [r for r in cursor]
        words = []
        for x in range(db_info.__len__()):
            template_phonemes = []
            query_temp_phonemes = "SELECT word_phoneme_number,phoneme_text,stress_id,tobi_pitch_accent FROM " \
                                  "template_words_phonemes WHERE template_id=" + str(template_id) + " AND word_number=" + str(db_info[x][1])
            cursor.execute(query_temp_phonemes)
            db_phonemes = [r for r in cursor]
            for y in range(db_phonemes.__len__()):
                template_phonemes.append({"phoneme_number": db_phonemes[y][0], "phoneme": db_phonemes[y][1], "stress_type": db_phonemes[y][2], "pitch_type": db_phonemes[y][3]})
            template_words = {
                "word_number": db_info[x][1],
                "sub_field_id": 0 if db_info[x][2] is None else db_info[x][2],
                "field_type_id": 0 if db_info[x][9] is None else db_info[x][9],
                "pos_code": db_info[x][4],
                "boundary_tone_id": db_info[x][5],
                "phrase_break_id": db_info[x][6],
                "focus": db_info[x][7],
                "compressed_f0": True if db_info[x][8] == 1 else False,
                "text": db_info[x][3],
                "sensitivity_next_word": True if db_info[x][12] == 1 else False,
                "sensitivity_gender": True if db_info[x][10] == 1 else False,
                "mood_id": 0 if db_info[x][11] is None else db_info[x][11],"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100",
                "phonemes": template_phonemes}
            words.append(template_words)

        # Set user parameters
        language_id = ''.join(random.choice(string.ascii_letters) for ii in range(6))
        user = {"template_id": template_id,"domain_id": domain_id, "template_name": template_name, "is_public": is_public, "text_ssml": text_ssml
                , "language_id": language_id, "project_id": project_id, "words": words}

        # Send request
        resp = Renderer_Functions.send_request(cookie, user, "templateupdate")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp['status_code'], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"], "incorrect value for language_id.") is None):
            print("resp: {0}".format(resp))
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_4_updateTemplate_missing_language_id(self):

                 # Login and get token
                resp = Renderer_Functions.initiate_login_admin()
                cookie = resp["cookies"][0]
                email = resp['content']['results']['user']['email_address']
                cursor = Postgres.connect_to_db()

                # Get random request data
                # Get template ids
                query_templates = "SELECT template_id,smi_domain_id FROM template WHERE template_id IN (SELECT DISTINCT template_id FROM renderer_text where template_id in( SELECT  renderer_text.template_id FROM renderer_text GROUP BY template_id HAVING COUNT(renderer_text_id) < 2))"
                cursor.execute(query_templates)
                db_info = [r for r in cursor]
                index = random.randint(0, db_info.__len__())
                template_id = db_info[index][0]
                domain_id = db_info[index][1]

                # Get text_ssml,template_name,open_to_public values
                query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id=" + str(
                    template_id) + " AND smi_domain_id=" + str(domain_id)
                cursor.execute(query_renderertext)
                db_info = [r for r in cursor]
                template_name = db_info[0][0]
                text_ssml = db_info[0][1]
                is_public = db_info[0][2]

                # Get project_id and language_id values
                query_domain = "SELECT smi_domain_owner_project_id,language_id FROM smi_domain WHERE smi_domain_id=" + str(
                    domain_id)
                cursor.execute(query_domain)
                db_info = [r for r in cursor]
                project_id = db_info[0][0]
                language_id = db_info[0][1]

                # Get template words
                query_temp_words = "SELECT  * FROM template_words WHERE template_id=" + str(template_id)
                cursor.execute(query_temp_words)
                db_info = [r for r in cursor]
                words = []
                for x in range(db_info.__len__()):
                    template_phonemes = []
                    query_temp_phonemes = "SELECT word_phoneme_number,phoneme_text,stress_id,tobi_pitch_accent FROM " \
                                          "template_words_phonemes WHERE template_id=" + str(
                        template_id) + " AND word_number=" + str(db_info[x][1])
                    cursor.execute(query_temp_phonemes)
                    db_phonemes = [r for r in cursor]
                    for y in range(db_phonemes.__len__()):
                        template_phonemes.append({"phoneme_number": db_phonemes[y][0], "phoneme": db_phonemes[y][1],
                                              "stress_type": db_phonemes[y][2], "pitch_type": db_phonemes[y][3]})
                    template_words = {
                        "word_number": db_info[x][1],
                        "sub_field_id": 0 if db_info[x][2] is None else db_info[x][2],
                        "field_type_id": 0 if db_info[x][9] is None else db_info[x][9],
                        "pos_code": db_info[x][4],
                        "boundary_tone_id": db_info[x][5],
                        "phrase_break_id": db_info[x][6],
                        "focus": db_info[x][7],
                        "compressed_f0": True if db_info[x][8] == 1 else False,
                        "text": db_info[x][3],
                        "sensitivity_next_word": True if db_info[x][12] == 1 else False,
                        "sensitivity_gender": True if db_info[x][10] == 1 else False,
                        "mood_id": 0 if db_info[x][11] is None else db_info[x][11],"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100",
                        "phonemes": template_phonemes}
                    words.append(template_words)

                # Set user parameters
                language_id = ''.join(random.choice(string.ascii_letters) for ii in range(6))
                user = {"template_id": template_id, "domain_id": domain_id, "template_name": template_name,
                        "is_public": is_public, "text_ssml": text_ssml
                    , "project_id": project_id, "words": words}

                # Send request
                resp = Renderer_Functions.send_request(cookie, user, "templateupdate")

                # here we will check if the request is passed or failed
                if (self.assertEqual(resp['status_code'], 200)) is None \
                        and (
                        self.assertEqual(resp["results"]["error_message"], "missing parameter language_id.") is None):
                    print("resp: {0}".format(resp))
                else:
                    print("Negative Test Failed")
                    assert False


    def test_negative_5_updateTemplate_incorrect_template_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']
        cursor = Postgres.connect_to_db()

        # Get random request data
        # Get template ids
        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE template_id IN (SELECT DISTINCT template_id FROM renderer_text where template_id in( SELECT  renderer_text.template_id FROM renderer_text GROUP BY template_id HAVING COUNT(renderer_text_id) < 2))"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0,db_info.__len__())
        template_id = db_info[index][0]
        domain_id = db_info[index][1]

        # Get text_ssml,template_name,open_to_public values
        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id="+str(template_id)+" AND smi_domain_id="+str(domain_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]
        is_public = db_info[0][2]

        # Get project_id and language_id values
        query_domain = "SELECT smi_domain_owner_project_id,language_id FROM smi_domain WHERE smi_domain_id="+str(domain_id)
        cursor.execute(query_domain)
        db_info = [r for r in cursor]
        project_id = db_info[0][0]
        language_id = db_info[0][1]

        # Get template words
        query_temp_words = "SELECT  * FROM template_words WHERE template_id="+str(template_id)
        cursor.execute(query_temp_words)
        db_info = [r for r in cursor]
        words = []
        for x in range(db_info.__len__()):
            template_phonemes = []
            query_temp_phonemes = "SELECT word_phoneme_number,phoneme_text,stress_id,tobi_pitch_accent FROM " \
                                  "template_words_phonemes WHERE template_id=" + str(template_id) + " AND word_number=" + str(db_info[x][1])
            cursor.execute(query_temp_phonemes)
            db_phonemes = [r for r in cursor]
            for y in range(db_phonemes.__len__()):
                template_phonemes.append({"phoneme_number": db_phonemes[y][0], "phoneme": db_phonemes[y][1], "stress_type": db_phonemes[y][2], "pitch_type": db_phonemes[y][3]})
            template_words = {
                "word_number": db_info[x][1],
                "sub_field_id": 0 if db_info[x][2] is None else db_info[x][2],
                "field_type_id": 0 if db_info[x][9] is None else db_info[x][9],
                "pos_code": db_info[x][4],
                "boundary_tone_id": db_info[x][5],
                "phrase_break_id": db_info[x][6],
                "focus": db_info[x][7],
                "compressed_f0": True if db_info[x][8] == 1 else False,
                "text": db_info[x][3],
                "sensitivity_next_word": True if db_info[x][12] == 1 else False,
                "sensitivity_gender": True if db_info[x][10] == 1 else False,
                "mood_id": 0 if db_info[x][11] is None else db_info[x][11],"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100",
                "phonemes": template_phonemes}
            words.append(template_words)

        # Set user parameters
        template_id = ''.join(random.choice(string.ascii_letters) for ii in range(6))
        user = {"template_id": template_id ,"domain_id": domain_id, "template_name": template_name, "is_public": is_public, "text_ssml": text_ssml
                , "language_id": language_id, "project_id": project_id, "words": words}

        # Send request
        resp = Renderer_Functions.send_request(cookie, user, "templateupdate")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp['status_code'], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"],"incorrect value for template_id.") is None):
            print("resp: {0}".format(resp))
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_5_updateTemplate_missing_template_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']
        cursor = Postgres.connect_to_db()

        # Get random request data
        # Get template ids
        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE template_id IN (SELECT DISTINCT template_id FROM renderer_text where template_id in( SELECT  renderer_text.template_id FROM renderer_text GROUP BY template_id HAVING COUNT(renderer_text_id) < 2))"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0, db_info.__len__())
        template_id = db_info[index][0]
        domain_id = db_info[index][1]

        # Get text_ssml,template_name,open_to_public values
        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id=" + str(
            template_id) + " AND smi_domain_id=" + str(domain_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]
        is_public = db_info[0][2]

        # Get project_id and language_id values
        query_domain = "SELECT smi_domain_owner_project_id,language_id FROM smi_domain WHERE smi_domain_id=" + str(
            domain_id)
        cursor.execute(query_domain)
        db_info = [r for r in cursor]
        project_id = db_info[0][0]
        language_id = db_info[0][1]

        # Get template words
        query_temp_words = "SELECT  * FROM template_words WHERE template_id=" + str(template_id)
        cursor.execute(query_temp_words)
        db_info = [r for r in cursor]
        words = []
        for x in range(db_info.__len__()):
            template_phonemes = []
            query_temp_phonemes = "SELECT word_phoneme_number,phoneme_text,stress_id,tobi_pitch_accent FROM " \
                                  "template_words_phonemes WHERE template_id=" + str(
                template_id) + " AND word_number=" + str(db_info[x][1])
            cursor.execute(query_temp_phonemes)
            db_phonemes = [r for r in cursor]
            for y in range(db_phonemes.__len__()):
                template_phonemes.append({"phoneme_number": db_phonemes[y][0], "phoneme": db_phonemes[y][1],
                                      "stress_type": db_phonemes[y][2], "pitch_type": db_phonemes[y][3]})
            template_words = {
                "word_number": db_info[x][1],
                "sub_field_id": 0 if db_info[x][2] is None else db_info[x][2],
                "field_type_id": 0 if db_info[x][9] is None else db_info[x][9],
                "pos_code": db_info[x][4],
                "boundary_tone_id": db_info[x][5],
                "phrase_break_id": db_info[x][6],
                "focus": db_info[x][7],
                "compressed_f0": True if db_info[x][8] == 1 else False,
                "text": db_info[x][3],
                "sensitivity_next_word": True if db_info[x][12] == 1 else False,
                "sensitivity_gender": True if db_info[x][10] == 1 else False,
                "mood_id": 0 if db_info[x][11] is None else db_info[x][11],"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100",
                "phonemes": template_phonemes}
            words.append(template_words)

        # Set user parameters
        user = {"domain_id": domain_id, "template_name": template_name,
                "is_public": is_public, "text_ssml": text_ssml
            , "project_id": project_id,"language_id": language_id, "words": words}

        # Send request
        resp = Renderer_Functions.send_request(cookie, user, "templateupdate")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp['status_code'], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"], "missing parameter template_id.") is None) :

            print("resp: {0}".format(resp))
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_6_updateTemplate_missing_project_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']
        cursor = Postgres.connect_to_db()

        # Get random request data
        # Get template ids
        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE template_id IN (SELECT DISTINCT template_id FROM renderer_text where template_id in( SELECT  renderer_text.template_id FROM renderer_text GROUP BY template_id HAVING COUNT(renderer_text_id) < 2))"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0, db_info.__len__())
        template_id = db_info[index][0]
        domain_id = db_info[index][1]

        # Get text_ssml,template_name,open_to_public values
        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id=" + str(
            template_id) + " AND smi_domain_id=" + str(domain_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]
        is_public = db_info[0][2]

        # Get project_id and language_id values
        query_domain = "SELECT smi_domain_owner_project_id,language_id FROM smi_domain WHERE smi_domain_id=" + str(
            domain_id)
        cursor.execute(query_domain)
        db_info = [r for r in cursor]
        project_id = db_info[0][0]
        language_id = db_info[0][1]

        # Get template words
        query_temp_words = "SELECT  * FROM template_words WHERE template_id=" + str(template_id)
        cursor.execute(query_temp_words)
        db_info = [r for r in cursor]
        words = []
        for x in range(db_info.__len__()):
            template_phonemes = []
            query_temp_phonemes = "SELECT word_phoneme_number,phoneme_text,stress_id,tobi_pitch_accent FROM " \
                                  "template_words_phonemes WHERE template_id=" + str(
                template_id) + " AND word_number=" + str(db_info[x][1])
            cursor.execute(query_temp_phonemes)
            db_phonemes = [r for r in cursor]
            for y in range(db_phonemes.__len__()):
                template_phonemes.append({"phoneme_number": db_phonemes[y][0], "phoneme": db_phonemes[y][1],
                                      "stress_type": db_phonemes[y][2], "pitch_type": db_phonemes[y][3]})
            template_words = {
                "word_number": db_info[x][1],
                "sub_field_id": 0 if db_info[x][2] is None else db_info[x][2],
                "field_type_id": 0 if db_info[x][9] is None else db_info[x][9],
                "pos_code": db_info[x][4],
                "boundary_tone_id": db_info[x][5],
                "phrase_break_id": db_info[x][6],
                "focus": db_info[x][7],
                "compressed_f0": True if db_info[x][8] == 1 else False,
                "text": db_info[x][3],
                "sensitivity_next_word": True if db_info[x][12] == 1 else False,
                "sensitivity_gender": True if db_info[x][10] == 1 else False,
                "mood_id": 0 if db_info[x][11] is None else db_info[x][11],"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100",
                "phonemes": template_phonemes}
            words.append(template_words)

        # Set user parameters
        user = {"domain_id": domain_id, "template_name": template_name,
                "is_public": is_public, "text_ssml": text_ssml
            ,"template_id": template_id, "language_id": language_id, "words": words}

        # Send request
        resp = Renderer_Functions.send_request(cookie, user, "templateupdate")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp['status_code'], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"], "missing parameter project_id.") is None):

            print("resp: {0}".format(resp))
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_6_updateTemplate_incorrect_project_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']
        cursor = Postgres.connect_to_db()

        # Get random request data
        # Get template ids
        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE template_id IN (SELECT DISTINCT template_id FROM renderer_text where template_id in( SELECT  renderer_text.template_id FROM renderer_text GROUP BY template_id HAVING COUNT(renderer_text_id) < 2))"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0,db_info.__len__())
        template_id = db_info[index][0]
        domain_id = db_info[index][1]

        # Get text_ssml,template_name,open_to_public values
        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id="+str(template_id)+" AND smi_domain_id="+str(domain_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]
        is_public = db_info[0][2]

        # Get project_id and language_id values
        query_domain = "SELECT smi_domain_owner_project_id,language_id FROM smi_domain WHERE smi_domain_id="+str(domain_id)
        cursor.execute(query_domain)
        db_info = [r for r in cursor]
        project_id = db_info[0][0]
        language_id = db_info[0][1]

        # Get template words
        query_temp_words = "SELECT  * FROM template_words WHERE template_id="+str(template_id)
        cursor.execute(query_temp_words)
        db_info = [r for r in cursor]
        words = []
        for x in range(db_info.__len__()):
            template_phonemes = []
            query_temp_phonemes = "SELECT word_phoneme_number,phoneme_text,stress_id,tobi_pitch_accent FROM " \
                                  "template_words_phonemes WHERE template_id=" + str(template_id) + " AND word_number=" + str(db_info[x][1])
            cursor.execute(query_temp_phonemes)
            db_phonemes = [r for r in cursor]
            for y in range(db_phonemes.__len__()):
                template_phonemes.append({"phoneme_number": db_phonemes[y][0], "phoneme": db_phonemes[y][1], "stress_type": db_phonemes[y][2], "pitch_type": db_phonemes[y][3]})
            template_words = {
                "word_number": db_info[x][1],
                "sub_field_id": 0 if db_info[x][2] is None else db_info[x][2],
                "field_type_id": 0 if db_info[x][9] is None else db_info[x][9],
                "pos_code": db_info[x][4],
                "boundary_tone_id": db_info[x][5],
                "phrase_break_id": db_info[x][6],
                "focus": db_info[x][7],
                "compressed_f0": True if db_info[x][8] == 1 else False,
                "text": db_info[x][3],
                "sensitivity_next_word": True if db_info[x][12] == 1 else False,
                "sensitivity_gender": True if db_info[x][10] == 1 else False,
                "mood_id": 0 if db_info[x][11] is None else db_info[x][11],"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100",
                "phonemes": template_phonemes}
            words.append(template_words)

        # Set user parameters
        project_id = ''.join(random.choice(string.ascii_letters) for ii in range(6))
        user = {"template_id": template_id,"domain_id": domain_id, "template_name": template_name, "is_public": is_public, "text_ssml": text_ssml
                , "language_id": language_id, "project_id": project_id, "words": words}

        # Send request
        resp = Renderer_Functions.send_request(cookie, user, "templateupdate")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp['status_code'], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"], "incorrect project_id") is None):
            print("resp: {0}".format(resp))
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_7_updateTemplate_Template_not_in_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']
        cursor = Postgres.connect_to_db()

        # Get random request data
        # Get template ids
        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE template_id IN (SELECT DISTINCT template_id FROM renderer_text where template_id in( SELECT  renderer_text.template_id FROM renderer_text GROUP BY template_id HAVING COUNT(renderer_text_id) < 2))"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0,db_info.__len__())
        template_id = db_info[index][0]
        domain_id = db_info[index][1]

        # Get text_ssml,template_name,open_to_public values
        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id="+str(template_id)+" AND smi_domain_id="+str(domain_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]
        is_public = db_info[0][2]

        # Get template_id not in the domain
        query_renderertext2 = "select template_id from template where smi_domain_id !=" + str(domain_id) + "And template_id IN (SELECT template_id FROM renderer_text GROUP BY template_id HAVING COUNT(renderer_text_id) < 2)"
        cursor.execute(query_renderertext2)
        db_info = [r for r in cursor]
        index = random.randint(0, db_info.__len__())
        template_id2 = db_info[index][0]

        # Get project_id and language_id values
        query_domain = "SELECT smi_domain_owner_project_id,language_id FROM smi_domain WHERE smi_domain_id="+str(domain_id)
        cursor.execute(query_domain)
        db_info = [r for r in cursor]
        project_id = db_info[0][0]
        language_id = db_info[0][1]

        # Get template words
        query_temp_words = "SELECT  * FROM template_words WHERE template_id="+str(template_id)
        cursor.execute(query_temp_words)
        db_info = [r for r in cursor]
        words = []
        for x in range(db_info.__len__()):
            template_phonemes = []
            query_temp_phonemes = "SELECT word_phoneme_number,phoneme_text,stress_id,tobi_pitch_accent FROM " \
                                  "template_words_phonemes WHERE template_id=" + str(template_id) + " AND word_number=" + str(db_info[x][1])
            cursor.execute(query_temp_phonemes)
            db_phonemes = [r for r in cursor]
            for y in range(db_phonemes.__len__()):
                template_phonemes.append({"phoneme_number": db_phonemes[y][0], "phoneme": db_phonemes[y][1], "stress_type": db_phonemes[y][2], "pitch_type": db_phonemes[y][3]})
            template_words = {
                "word_number": db_info[x][1],
                "sub_field_id": 0 if db_info[x][2] is None else db_info[x][2],
                "field_type_id": 0 if db_info[x][9] is None else db_info[x][9],
                "pos_code": db_info[x][4],
                "boundary_tone_id": db_info[x][5],
                "phrase_break_id": db_info[x][6],
                "focus": db_info[x][7],
                "compressed_f0": True if db_info[x][8] == 1 else False,
                "text": db_info[x][3],
                "sensitivity_next_word": True if db_info[x][12] == 1 else False,
                "sensitivity_gender": True if db_info[x][10] == 1 else False,
                "mood_id": 0 if db_info[x][11] is None else db_info[x][11],"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100",
                "phonemes": template_phonemes}
            words.append(template_words)

        # Set user parameters
        user = {"template_id": template_id2,"domain_id": domain_id, "template_name": template_name, "is_public": is_public, "text_ssml": text_ssml
                , "language_id": language_id, "project_id": project_id, "words": words}

        # Send request
        resp = Renderer_Functions.send_request(cookie, user, "templateupdate")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp['status_code'], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"], "template with id "+ str(template_id2)+" not in the domain "+str(domain_id)) is None):
            print("resp: {0}".format(resp))
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_8_updateTemplate_rendere_text_Template(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']
        cursor = Postgres.connect_to_db()


        # allowed_domains = Renderer_Functions.get_allowed_domains_in_allowed_projects_in_allowed_accounts(email,language_id)
        # domain_id = random.choice(allowed_domains)
        # domain_id2 = allowed_domains.remove(domain_id)

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(
            random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])
        today = datetime.date.today()

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))+"- "+str(today)
        #template_name = "testing template name " + "- " + str(today)

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6


        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(
            domain_id) + '"' \
                         ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                  "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample",
                  "sensitivity_next_word": False,
                  "sensitivity_gender": False, "mood_id": 0, "prosody_speed": "100", "prosody_volume": "100",
                  "prosody_pitch": "100", "phonemes": [
                {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
            ]
                  },
                 {
                     "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                     "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text",
                     "sensitivity_next_word": False,
                     "sensitivity_gender": False, "mood_id": 0, "prosody_speed": "100", "prosody_volume": "100",
                     "prosody_pitch": "100", "phonemes": [
                     {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                     {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                     {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                     {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                 ]
                 }]

        template_original = "NULL"
        # Add a template like you did. with template/add
        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,"is_public": is_public,"template_original": template_original, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # Fetch the info from the Database
        cursor = Postgres.connect_to_db()
        query = "select * from renderer_text where name ilike '" + template_name + "'and is_template_org = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        template_id = db_info[0][6]
        domain_id=db_info[0][8]
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["template_id"], str(db_info[0][6])) is None):
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False
        print("Templates Add Pass")


        # Get text_ssml,template_name,open_to_public values
        query_renderertext = "SELECT renderer_text_id, name,text_ssml,open_to_public,text_owner_project_id,normalization_domain_id FROM renderer_text WHERE is_template_org" \
                             " AND template_id=" + str(template_id) + " AND smi_domain_id=" + str(domain_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        text_id =db_info[0][0]
        template_name = db_info[0][1]
        text_ssml = db_info[0][2]
        is_public = db_info[0][3]
        #project_id =db_info[0][4]
        normalization_domain= db_info[0][5]


        # query_renderertext = "SELECT renderer_text_id, name,smi_domain_id,text_ssml,open_to_public,text_owner_project_id,normalization_domain_id FROM renderer_text WHERE " \
        #                      " is_template_org  AND smi_domain_id <>" + str(domain_id)
        # cursor.execute(query_renderertext)
        # db_info = [r for r in cursor]
        # text_id = db_info[0][0]
        # text_name = db_info[0][1]
        # domain_id = db_info[0][2]

        text_name=template_name
        query_renderertext = "SELECT domain_name FROM smi_domain WHERE smi_domain_id=" + str(domain_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        domain_name = db_info[0][0]


        template_name = "testing template name " + "- " + str(today)
        text_name=template_name

        #Use the API smorphing/2.0/savetext with request parameters
        # Set user parameters
        user = {"text_ssml": text_ssml, "project_id": project_id, "smi_domain_id":domain_id,
                "open_to_public":is_public, "renderer_text_id":text_id,"normalization_domain_id":normalization_domain,
                "domain_name":domain_name, "template_original":"NULL", "smi_template_id":template_id,"template_id":template_id,
                "name":text_name }
        resp2 = Renderer_Functions.send_request(cookie, user, "savetext")


        #use another domain_id
        query_renderertext = "SELECT smi_domain_id FROM renderer_text WHERE smi_domain_id <> " + str(domain_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        index = random.randint(0,db_info.__len__())
        domain_id_2 = db_info[index][0]
        user = {"template_id": template_id, "domain_id": domain_id_2, "template_name": template_name,
                "is_public": is_public,"render_text_id":text_id, "text_ssml": text_ssml
            , "language_id": language_id, "project_id": project_id, "words": words}

        # Send request
        resp3 = Renderer_Functions.send_request(cookie, user, "templateupdate")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp2['status_code'], 200)) is None \
                and (self.assertEqual(resp3["results"]["error_message"], "cannot update template with template_id "+str(template_id)+". Please add as a new template.") is None):
            print("resp: {0}".format(resp))
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_9_updateTemplate_mismatch_sub_field(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']
        cursor = Postgres.connect_to_db()

        # Get random request data
        # Get template ids
        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE template_id IN (SELECT DISTINCT template_id FROM renderer_text where template_id in( SELECT  renderer_text.template_id FROM renderer_text GROUP BY template_id HAVING COUNT(renderer_text_id) < 2))"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0,db_info.__len__())
        template_id = db_info[index][0]
        domain_id = db_info[index][1]

        # Get text_ssml,template_name,open_to_public values
        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id="+str(template_id)+" AND smi_domain_id="+str(domain_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]
        is_public = db_info[0][2]

        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" ' + \
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that ' + \
        #             'is open on a holiday? <say-as interpret-as="sub-field" detail="5"> you?</say-as> </speak>'

        # Get project_id and language_id values
        query_domain = "SELECT smi_domain_owner_project_id,language_id FROM smi_domain WHERE smi_domain_id="+str(domain_id)
        cursor.execute(query_domain)
        db_info = [r for r in cursor]
        project_id = db_info[0][0]
        language_id = db_info[0][1]

        # Get template words
        #query_temp_words = "SELECT  * FROM template_words WHERE template_id="+str(template_id)+"AND sub_field_id !="+'5'
        query_temp_words = "SELECT  * FROM template_words WHERE template_id=" + str(template_id)
        cursor.execute(query_temp_words)
        db_info = [r for r in cursor]
        words = []
        for x in range(db_info.__len__()):
            template_phonemes = []
            query_temp_phonemes = "SELECT word_phoneme_number,phoneme_text,stress_id,tobi_pitch_accent FROM " \
                                  "template_words_phonemes WHERE template_id=" + str(template_id) + " AND word_number=" + str(db_info[x][1])
            cursor.execute(query_temp_phonemes)
            db_phonemes = [r for r in cursor]
            for y in range(db_phonemes.__len__()):
                template_phonemes.append({"phoneme_number": db_phonemes[y][0], "phoneme": db_phonemes[y][1], "stress_type": db_phonemes[y][2], "pitch_type": db_phonemes[y][3]})
            template_words = {
                "word_number": db_info[x][1],
                "sub_field_id": 121 if db_info[x][2] is None else db_info[x][2],
                "field_type_id": 0 if db_info[x][9] is None else db_info[x][9],
                "pos_code": db_info[x][4],
                "boundary_tone_id": db_info[x][5],
                "phrase_break_id": db_info[x][6],
                "focus": db_info[x][7],
                "compressed_f0": True if db_info[x][8] == 1 else False,
                "text": db_info[x][3],
                "sensitivity_next_word": True if db_info[x][12] == 1 else False,
                "sensitivity_gender": True if db_info[x][10] == 1 else False,
                "mood_id": 0 if db_info[x][11] is None else db_info[x][11],"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100",
                "phonemes": template_phonemes}
            words.append(template_words)


        # Set user parameters
        user = {"template_id": template_id, "domain_id": domain_id,"template_name": template_name, "is_public": is_public, "text_ssml": text_ssml
                , "language_id": language_id, "project_id": project_id, "words": words}

        # Send request
        resp = Renderer_Functions.send_request(cookie, user, "templateupdate")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp['status_code'], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"], "sub field id in text_ssml are not matched with sub_field_id the words. ") is None):
            print("resp: {0}".format(resp))
        else:
            print("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
