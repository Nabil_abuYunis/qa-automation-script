import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint
import configparser
from Database import Postgres

class VBMoodSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    def test_positive_1_mood(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    # Parameters
                    user = {"smi_domain_id": "72057594037927948"}
                    resp = Renderer_Functions.mood(resp["cookies"][0], user)
                    count = resp['count']
                    # Fetch the info from Database
                    # Needs to check for inherited moods as well, doesn't currently
                    query = "SELECT smi_domain_id, mood_id "+\
                            "FROM public.smi_domain_allowed_moods "+\
                            "WHERE smi_domain_id =" + user['smi_domain_id'] +\
                            " AND mood_id IN (SELECT mood_id FROM mood WHERE supported) "+\
                            "ORDER BY 2;"
                    cursor = Postgres.connect_to_db()
                    cursor.execute(query)
                    db_info = [r for r in cursor]
                    
                    quark = "WITH RECURSIVE parents(c_id, p_id, level) as (" +\
                            "SELECT smi_domain_id, parent_id, 0 FROM smi_domain WHERE smi_domain_id = " + user['smi_domain_id'] +\
                            " UNION ALL " +\
                            "SELECT smi_domain_id, parent_id, level +1 " +\
                            "FROM smi_domain " +\
                            "INNER JOIN parents ON (smi_domain_id = p_id)) " +\
                            "SELECT smi_domain_id, domain_name, innerQuery.mood_id, name " +\
                            "FROM    smi_domain " +\
                            "INNER JOIN(SELECT smi_domain_id, mood_id, level, rank() OVER(PARTITION BY mood_id ORDER BY level), name " +\
                            "FROM smi_domain_allowed_moods "+\
                            "INNER JOIN parents ON(smi_domain_id = c_id) "+\
                            "INNER JOIN (SELECT mood_id, name FROM mood WHERE supported) mood USING (mood_id) " +\
                            ") innerQuery USING (smi_domain_id) "+\
                            "WHERE rank = 1 " +\
                            "AND level > 0 " +\
                            "ORDER BY 2;"

                    rohana = Postgres.connect_to_db()
                    rohana.execute(quark)
                    amon = [r for r in rohana]
                    
                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    resp = sorted(resp["results"], key=lambda k: k['mood_id'])
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    
                    #print(db_info[random_line])
                    convert = "SELECT mood_id, name, color, supported FROM mood WHERE mood_id = " + str(resp[random_line]["mood_id"])
                    cursor2 = Postgres.connect_to_db()
                    cursor2.execute(convert)
                    
                    cursor3 = Postgres.connect_to_db()
                    cursor3.execute("SELECT domain_name FROM smi_domain WHERE smi_domain_id = " + str(resp[random_line]["smi_domain_id"]))

                    mood_info = [r for r in cursor2]
                    domain_info = [r for r in cursor3]

                    beer = amon + db_info
                    print("project the keystone!")
                    print(beer)
                    for x in beer:
                        if resp[random_line]["smi_domain_id"] == x[0]:
                            # here we will check if the request is passed or failed
                            if (self.assertEqual(resp_status_code, 200)) is None \
                                    and (self.assertEqual(resp[random_line]["color"], mood_info[0][2]) is None) \
                                    and (self.assertEqual(resp[random_line]["supported"], mood_info[0][3]) is None) \
                                    and (self.assertEqual(resp[random_line]["mood_id"], str(mood_info[0][0])) is None) \
                                    and (self.assertEqual(resp[random_line]["domain_name"], domain_info[0][0]) is None) \
                                    and (self.assertEqual(resp[random_line]["smi_domain_id"], x[0]) is None) \
                                    and (self.assertEqual(resp[random_line]["name"], mood_info[0][1]) is None):
                                print("Mood Pass")
                                print("resp: {0}".format(resp))
                                print("Color: {0}".format(resp[random_line]["color"]))
                                print("Supported: {0}".format(resp[random_line]["supported"]))
                                print("Mood_Id: {0}".format(resp[random_line]["mood_id"]))
                                print("Name: {0}".format(resp[random_line]["name"]))
                                print("Domain: {0}".format(resp[random_line]["domain_name"]))
                                print("Domain Id: {0}".format(resp[random_line]["smi_domain_id"]))
                            

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_2_mood_missing_sendUnsupported(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    # Parameters
                    user = {}
                    resp = Renderer_Functions.mood(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error_message"],
                                                          "Missing Parameter smi_domain_id.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error_message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_3_mood_wrong_login(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.mood("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_4_mood_missing_login(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.mood("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")


    def test_negative_5_mood_incorrect_domain(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    # Parameters
                    user = {"smi_domain_id" : "old"}
                    resp = Renderer_Functions.mood(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error_message"],
                                                          "Incorrect value for smi_domain_id.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error_message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.mood("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
