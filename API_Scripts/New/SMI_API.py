from random import randint

def generate_header(token):
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    return headers

def get_rand_voice(cursor):
    cursor.execute(
      "WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = 'hung@speechmorphing.com' OR user_email ILIKE 'hung@speechmorphing.com') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name")

    db_info = [r for r in cursor]
    db_info = sorted(db_info, key=lambda k: k[0])
    random_line = randint(0, len(db_info) - 1)
    return db_info[random_line][0]

def endpoint_switch(url, name):
    if name is not None:
        switcher = {
            'logout': "/smorphing/1.0/logout",
            'voicedetails' : "/smorphing/1.0/voicedetails",
            'voices' : "/smorphing/1.0/voices",
            'domain' : "/smorphing/1.0/domain",
            'templates' : "/smorphing/1.0/saytemplate/file",
            'say' : "/smorphing/1.0/say/file",
            'login' : "/smorphing/1.0/login",
            'language' : "/smorphing/1.0/language",
            'age' : "/smorphing/1.0/age",
            'gender' : "/smorphing/1.0/gender",
            'svp' : "/smorphing/1.0/svp",
            'gesture' : "/smorphing/1.0/domain/allowed_gestures/list",
            'mood' : "/smorphing/1.0/domain/allowed_moods/list",
            'style' : "/smorphing/1.0/domain/allowed_styles/list",
            'filenames' : "/smorphing/1.0/filenames",
            'text' : "/smorphing/1.0/text",
            'signup' : "/smorphing/1.0/signup",
            'keyword' : "/smorphing/1.0/domain/keywords/list",
            'lingo' : "/smorphing/1.0/domain/lingo/list",
            'lexicon' : "/smorphing/1.0/definelexicon",
            'jump' : "/smorphing/1.0/jump",
            'stop' : "/smorphing/1.0/stop",
            'changerate' : "/smorphing/1.0/changerate",
            'cancel' : "/smorphing/1.0/cancel",
            'wordreplacement' : "/smorphing/1.0/domain/word_replacement/list",
            'wordreplacementadd' : "/smorphing/1.0/domain/word_replacement/add",
            'wordreplacementdel' : "/smorphing/1.0/domain/word_replacement/delete",
            'wordtranscription' : "/smorphing/1.0/domain/word_transcription/list",
            'wordtranscriptionadd' : "/smorphing/1.0/domain/word_transcription/add",
            'wordtranscriptiondel' : "/smorphing/1.0/domain/word_transcription/delete"
        }
    else:
        return None
    
    return url + switcher[name]
