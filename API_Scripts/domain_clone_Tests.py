import unittest
import Renderer_Functions
from Database import Postgres
import random
import string
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBDomainCloneSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)


    def test_positive_1_clone_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        language_id = 6
        domain_name = ''.join(random.choice(string.ascii_letters) for ii in range(6))
        domain_name = 'test_API' + domain_name

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        # allowed_domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, project_id)
        # smi_domain_id = random.choice(allowed_domains)[0]
        # Get domains with permissions for the project
        cursor = Postgres.connect_to_db()
        query = "SELECT smi_domain_id FROM smi_domain_permissions  WHERE project_id ILIKE '" + project_id + "'"
        cursor.execute(query)
        db_info = [r for r in cursor]
        index = random.randrange(0, db_info.__len__()-1)
        smi_domain_id = db_info[index][0]

        # project_id = "S4398046511105-4398046511106"
        # smi_domain_id = 1099511627783
        # domain_name = "UJhooWd"
        open_to_public = True

        user = {"domain_name": domain_name, "project_id": project_id, "smi_domain_id": smi_domain_id, "open_to_public": open_to_public}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domainclone")

        # Need permission question answered first

        # Fetch the info from the Database
        query = "select smi_domain_id,domain_name from smi_domain where domain_name ilike '" + domain_name + \
                "' and smi_domain_owner_project_id ilike '" + project_id + "'"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]

        # resp = sorted(resp["results"], key=lambda k: k[1])

        if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp["results"]['smi_domain_id'], str(db_info[0][0])) is None) \
                and (self.assertEqual(resp["results"]['domain_name'], db_info[0][1]) is None):
                print ("Domain clone Pass")
                print("resp: {0}".format(resp))
                print("domain_Id: {0}".format(resp["results"]['smi_domain_id']))
                print("domain Name: {0}".format(resp["results"]['domain_name']))

                f = open("Added_Parameters.txt", "a+")
                f.write("\n")
                f.write("domain_Id: {0} "+ resp["results"]['smi_domain_id'])
                f.write("\n")
                f.write("domain Name: {0} " + resp["results"]['domain_name'])
                f.write("\n")
        else:
                print("Positive Test Failed")
                assert False
        print("Domain clone Pass")

    def test_negative_2_clone_domain_wrong_login(self):
        # Parameters
        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        domain_name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        allowed_domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, project_id)
        smi_domain_id = random.choice(allowed_domains)[0]

        user = {"domain_name": domain_name, "project_id": project_id, "smi_domain_id": smi_domain_id}
        resp = Renderer_Functions.send_request("123456789", user, "domainclone")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_clone_domain_missing_login(self):
        # Parameters
        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        domain_name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        allowed_domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, project_id)
        smi_domain_id = random.choice(allowed_domains)[0]

        user = {"domain_name": domain_name, "project_id": project_id, "smi_domain_id": smi_domain_id}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "domainclone")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_clone_domain_expired_token(self):
        # Parameters
        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        domain_name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        allowed_domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, project_id)
        smi_domain_id = random.choice(allowed_domains)[0]

        user = {"domain_name": domain_name, "project_id": project_id, "smi_domain_id": smi_domain_id}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "domainclone")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
