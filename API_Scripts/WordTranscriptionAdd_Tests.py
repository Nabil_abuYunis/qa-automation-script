import unittest
import Renderer_Functions
from Database import Postgres
import random
import string
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBWordTranscriptionAddSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    #Need to change the word after each run as word will already be added
    def test_positive_1_word_transcription(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        #Parameters
        word = ''.join(random.choice(string.ascii_letters) for i in range(5))
        transcription = ''.join(random.choice(string.ascii_letters) for i in range(5))

        cursor = Postgres.connect_to_db()
        query = "select smi_domain_id from smi_domain where supported AND open_to_public"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_domain_id = random.choice(db_info)[0]
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_domain_id": smi_domain_id, "word": word, "transcription": transcription, "project_id": random_project}
        resp = Renderer_Functions.send_request(cookies, user, "wordtranscriptionadd")

        if (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["success_message"], "word '" + word +"' will be transcribed as '" + transcription + "'") is None):
            print("Word Transcription Add Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))
        else:
            print("Positive Test Failed")
            assert False

        print("Word Transcription Add Pass")

        # After adding the word, do remove them in order to leave the DB as it was
        Renderer_Functions.send_request(cookies, user, "wordtranscriptiondel")

    def test_negative_2_incorrect_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        #Parameters
        word = "bound"
        transcription = "verb"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_domain_id": "boom", "word": word, "transcription": transcription, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "wordtranscriptionadd")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "incorrect value for smi_domain_id.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_3_incorrect_word(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        #Parameters
        word = 16542
        transcription = "verb"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_domain_id": "72057594037927948", "word": word, "transcription": transcription, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "wordtranscriptionadd")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "incorrect value for word.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_4_incorrect_transcription(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        #Parameters
        cursor = Postgres.connect_to_db()
        query = "select smi_domain_id from smi_domain where supported AND open_to_public"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_domain_id = random.choice(db_info)[0]
        word = "forget"
        transcription = 1236879
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_domain_id": smi_domain_id, "word": word, "transcription": transcription, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "wordtranscriptionadd")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "incorrect value for transcription.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_5_missing_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        #Parameters
        word = "forget"
        transcription = "corporation"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"word": word, "transcription": transcription, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "wordtranscriptionadd")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "missing parameter smi_domain_id.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_6_missing_word(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        #Parameters
        transcription = "corporation"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_domain_id": "72057594037927948", "transcription": transcription, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "wordtranscriptionadd")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "missing parameter word.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_7_missing_transcription(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        #Parameters
        word = "forget"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_domain_id": "72057594037927948", "word":word, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "wordtranscriptionadd")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "missing parameter transcription.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_8_already_exists(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        # -------------------------------------
        # Send first new word - transcription
        # Parameters
        word = ''.join(random.choice(string.ascii_letters) for i in range(5))
        transcription = ''.join(random.choice(string.ascii_letters) for i in range(5))

        cursor = Postgres.connect_to_db()
        query = "select smi_domain_id from smi_domain where supported AND open_to_public"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_domain_id = random.choice(db_info)[0]
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_domain_id": smi_domain_id, "word": word, "transcription": transcription, "project_id": random_project}
        resp = Renderer_Functions.send_request(cookies, user, "wordtranscriptionadd")

        # -------------------------------------
        # Send again the added word - transcription
        resp = Renderer_Functions.send_request(cookies, user, "wordtranscriptionadd")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], " " + word + " already exists.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

        # After adding the word, do remove them in order to leave the DB as it was
        Renderer_Functions.send_request(cookies, user, "wordtranscriptiondel")

    def test_negative_9_permission_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        #Parameters
        cursor = Postgres.connect_to_db()

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Select smi_domain_id and their gestures gestures
        # Get domain_ids that user doesn't have permission to access
        query = "SELECT smi_domain_id FROM smi_domain WHERE smi_domain_owner_project_id <> '" + random_project + "' " \
                 "AND smi_domain_id NOT IN (SELECT smi_domain_id FROM smi_domain_permissions WHERE project_id ILIKE '" + random_project + "')"

        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_domain_id = random.choice(db_info)[0]

       # word = "forget"
        word = "remember"

        transcription = "corporation"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_domain_id": smi_domain_id, "word":word, "transcription":transcription, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "wordtranscriptionadd")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "you do not have permission to use this domain") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_10_word_transcription_wrong_login(self):

        email = self.config["LOGIN"]["email"]

        # parameters
        word = "bound"
        transcription = "verb"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_domain_id": "72057594037927948", "word": word, "transcription": transcription, "project_id": random_project}
        resp = Renderer_Functions.send_request("123456789", user, "wordtranscriptionadd")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_11_word_transcription_missing_login(self):
        email = self.config["LOGIN"]["email"]

        # parameters
        word = "bound"
        transcription = "verb"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_domain_id": "72057594037927948", "word": word, "transcription": transcription, "project_id": random_project}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "wordtranscriptionadd")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_12_expired_token(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        word = "bound"
        transcription = "verb"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_domain_id": "72057594037927948", "word": word, "transcription": transcription, "project_id": random_project}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "wordtranscriptionadd")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)