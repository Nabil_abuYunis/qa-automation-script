import glob
import unittest
import sys

class RunAllSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    test_type_default = "test"  # "1.test_type: positive: test_positive / negative: test_negative / all: test
    test_file_default = "*Tests.py"  # 2.test_file: all: *Tests.py / specific tests: *ge_Te*
    test_dir_default = "C:\\Users\\Dillon\\Documents\\qa-automation-script\\API_Scripts"  # 3.test_dir: the folder that contains the scripts

    # Command Line
    # python Run_All.py test *Tests.py C:\Users\mibo\Desktop\pyscripts
    print("COMMENCING RUN_ALL --PLEASE BE ADVISED")
    # Three parameters
    try:
        if "Run_All.py" not in sys.argv[1]:
            test_type = sys.argv[1]  # added "if" to bypass debugging software default parameters
        else: test_type = test_type_default
    except:
        test_type = test_type_default
    try:
        if sys.argv[2] != "true":
            test_file = sys.argv[2]
        else:
            test_file = test_file_default
    except:
        test_file = test_file_default
    try:
        test_dir = sys.argv[3]
    except:
        test_dir = test_dir_default
    del sys.argv[1:]

    # testsuite Array
    testsuite = unittest.TestSuite()
    print(testsuite)
    # 1. IMPORT THE FILES THAT GOING TO BE TESTED
    # get all the tests names
    test_file_strings = glob.glob(test_file)
    # clean extensions
    module_strings = [str[0:len(str) - 3] for str in test_file_strings]
    # import test files
    [__import__(str) for str in module_strings]

    # 2. FETCH THE REQUIRED METHODS FROM THE TEST FILES
    # fetch all the tests from the test files
    suites = []
    # fetch tests that includes specific prefix
    loader = unittest.TestLoader()
    loader.testMethodPrefix = test_type
    # fetch test files

    # for str in module_strings:
    suites.append(loader.discover(start_dir=test_dir, pattern=test_file))

    # adding the tests to the testsuite array
    for suite in suites:
      testsuite.addTest(suite)  

    # 3. RUN THE TEST FILES
    result = unittest.TestResult()
    testsuite.run(result)
