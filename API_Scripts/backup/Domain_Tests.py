import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
import string
from random import randint
import configparser
from Database import Postgres

class VBDomainSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Start run test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Completed running test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")
        print( self.currentResult)

    def test_positive_1_domain(self):
        # valid username and password
        email = self.config["LOGIN"]["email"]
        password = self.config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # Parameters
                    # Get Random Language id
                    cursor = Postgres.connect_to_db()
                    # select only the langauge_id's that have domains
                    cursor.execute('SELECT DISTINCT language_id FROM smi_domain')
                    db_info = [r for r in cursor]
                    random_line = randint(0, cursor.rowcount-1)

                    language_id = db_info[random_line][0]

                    # Send to Domain
                    user = {"language_id": language_id, "send_unsupported": True}
                    resp = Renderer_Functions.domain(resp["cookies"][0], user)

                    # Fetch the info from the domain table
                    cursor = Postgres.connect_to_db()
                    cursor.execute("WITH webSupported as (SELECT DISTINCT smi_domain_id FROM canned_clip_website WHERE supported)SELECT smi_domain_id,domain_name as name, language_id, parent_id as parent_smi_domain_id, normalization_domain_id, style_id, mood_id, speed, pitch, volume, open_to_public, supported, smi_domain_owner_email, string_agg(keyword, ', ') as keywords, string_agg(user_email, ', ') as email, websupported.smi_domain_id is not NULL web_supported   FROM smi_domain LEFT OUTER JOIN smi_domain_permissions USING (smi_domain_id) LEFT OUTER JOIN smi_domain_keywords USING (smi_domain_id) LEFT OUTER JOIN websupported using (smi_domain_id) WHERE (open_to_public OR smi_domain_owner_email = '" + email + "'  OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0  OR trim(domain_name) ~* '()' OR smi_domain_id IN (SELECT smi_domain_id FROM smi_domain_keywords  WHERE  keyword ~* '^()$')) THEN TRUE ELSE FALSE END GROUP BY smi_domain_id, domain_name, language_id, parent_id, normalization_domain_id, style_id, mood_id, speed, pitch, volume, open_to_public, smi_domain_owner_email, websupported.smi_domain_id   ORDER BY (websupported.smi_domain_id is NOT NULL) DESC, domain_name")

                    db_info = [r for r in cursor]
                    db_info = sorted(db_info, key=lambda k: k[0])
                    db_info = [x for x in db_info if x[2] == language_id]

                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    resp = sorted(resp["results"], key=lambda k: k["smi_domain_id"])

                    # Choose a random row to test
                    random_line = randint(0, len(db_info) - 1)
                    print(resp[random_line])
                    print(db_info[random_line])

                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp_status_code, 200) is None
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["smi_domain_id"],
                                                                             str(db_info[random_line][0])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["name"],
                                                                             str(db_info[random_line][1])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["mood_id"],
                                                                             str(db_info[random_line][6])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["open_to_public"],
                                                                             str(db_info[random_line][10])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["pitch"],
                                                                             str(db_info[random_line][8])))
                            and (
                            Renderer_Functions.compare_two_values_equal(resp[random_line]["smi_domain_owner_email"],
                                                                        str(db_info[random_line][12])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["supported"],
                                                                             str(db_info[random_line][11])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["volume"],
                                                                             str(db_info[random_line][9])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["style_id"],
                                                                             str(db_info[random_line][5])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["speed"],
                                                                             str(db_info[random_line][7])))
                            and (
                            Renderer_Functions.compare_two_values_equal(resp[random_line]["normalization_domain_id"],
                                                                            str(db_info[random_line][4])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["language_id"],
                                                                        str(db_info[random_line][2])))):
                        print ("Domain Pass")
                        print ("resp: {0}".format(resp))
                    else:
                        print( "Domain Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))

    def test_positive_8_open_to_public(self):
        # valid username and password
        email = self.config["LOGIN"]["email"]
        password = self.config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # Parameters
                    language_id = 6
                    send_unsupported = False
                    open_to_public = True
                    # Send to Domain
                    user = {"language_id": 6 , "send_unsupported": send_unsupported, "open_to_public": open_to_public}
                    resp = Renderer_Functions.domain(resp["cookies"][0], user)

                    # Fetch the info from the Age table
                    cursor = Postgres.connect_to_db()
                    cursor.execute("SELECT smi_domain_id, domain_name, parent_id, style_id, speed, pitch, volume, normalization_domain_id, open_to_public, mood_id, supported FROM smi_domain WHERE language_id=" + str(language_id) + " AND supported=" + str(not send_unsupported) + " AND open_to_public=" + str(open_to_public) + ";")

                    db_info = [r for r in cursor]
                    db_info = sorted(db_info, key=lambda k: k[0])
                    print(db_info)
                    print("------------")
                    print(len(db_info))
                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    count = resp["count"]
                    resp = sorted(resp["results"], key=lambda k: k["smi_domain_id"])

                    # Choose a random row to test
                    random_line = randint(0, int(count) - 1)

                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp_status_code, 200) is None
                            and (
                            Renderer_Functions.compare_two_values_equal(resp[random_line]["smi_domain_id"],
                                                                        str(db_info[random_line][0])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["name"],
                                                                             str(db_info[random_line][1])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["mood_id"],
                                                                             str(db_info[random_line][6])))
                            and (
                            Renderer_Functions.compare_two_values_equal(resp[random_line]["open_to_public"],
                                                                        str(db_info[random_line][10])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["pitch"],
                                                                             str(db_info[random_line][8])))
                            and (
                                    Renderer_Functions.compare_two_values_equal(
                                        resp[random_line]["smi_domain_owner_email"],
                                        str(db_info[random_line][12])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["supported"],
                                                                             str(db_info[random_line][11])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["volume"],
                                                                             str(db_info[random_line][9])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["style_id"],
                                                                             str(db_info[random_line][5])))
                            and (Renderer_Functions.compare_two_values_equal(resp[random_line]["speed"],
                                                                             str(db_info[random_line][7])))
                            and (
                                    Renderer_Functions.compare_two_values_equal(
                                        resp[random_line]["normalization_domain_id"],
                                        str(db_info[random_line][4])))
                            and (
                            Renderer_Functions.compare_two_values_equal(resp[random_line]["language_id"],
                                                                        str(db_info[random_line][2])))):
                        print( "Domain Pass")
                        print( "resp: {0}".format(resp))
                    else:
                        print("Couldn't find match domain")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))

    def test_negative_2_domain_incorrect_language_id(self):
        # valid username and password
        email = self.config["LOGIN"]["email"]
        password = self.config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # Parameters
                    language_id = 99999
                    user = {"language_id": language_id}
                    resp = Renderer_Functions.domain(resp["cookies"][0], user)
                    print(resp["status_code"])
                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Incorrect value for language_id.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))

    def test_negative_3_domain_missing_parameters(self):
        # valid username and password
        email = self.config["LOGIN"]["email"]
        password = self.config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # Parameters
                    user = {}
                    resp = Renderer_Functions.domain(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Missing Parameter language_id.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))

    def test_negative_4_domain_missing_language_id(self):
        # valid username and password
        email = self.config["LOGIN"]["email"]
        password = self.config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # Parameters
                    user = {"send_unsupported": True}
                    resp = Renderer_Functions.domain(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Missing Parameter language_id.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))

    '''
    def test_negative_5_domain_missing_sendUnsupported(self):
        ###############################################################
        #TEST CASE IS INVALID : send_unsupported is no longer mandatory
        ###############################################################
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # Parameters
                    user = {"language_id": 6}
                    resp = Renderer_Functions.domain(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Missing Parameter send_unsupported.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))
    '''

    def test_negative_6_domain_wrong_login(self):
        # Parameters
        user = {"language_id": 2, "send_unsupported": True}
        resp = Renderer_Functions.domain("123456789", user)
        print(resp)
        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_7_domain_missing_login(self):
        # Parameters
        user = {"language_id": 2, "send_unsupported": True}
        resp = Renderer_Functions.domain("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_8_expired_token(self):
        # Parameters
        user = {"language_id": 6, "send_unsupported": True}
        resp = Renderer_Functions.domain("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")
            

if __name__ == '__main__':
    print( "-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
