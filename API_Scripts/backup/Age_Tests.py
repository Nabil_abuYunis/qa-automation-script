import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
import configparser
from Database import Postgres

class VBAgeSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        print (self.currentResult)

    def test_positive_1_age(self):
        # config = configparser.ConfigParser()
        # config.sections()
        # config.read('api_config.ini')
        # # valid username and password
        # email = config["LOGIN"]["email"]
        # password = config["LOGIN"]["password"]
        # user = {"email": email, "password": password}
        # resp = Renderer_Functions.login(user)
        resp = Renderer_Functions.initiate_login();

        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.age(resp["cookies"][0], user)

        # Fetch the info from the Database
        cursor = Postgres.connect_to_db()
        cursor.execute('WITH webSupported as (SELECT DISTINCT age_id FROM smi_voice where smi_voice_id IN '
                       '(SELECT DISTINCT smi_voice_id FROM  canned_clip_website WHERE supported)) select '
                       'age_id, name, supported, (websupported.age_id is not NULL OR age_id = 0) '
                       'web_supported from age LEFT OUTER JOIN websupported using (age_id) ORDER BY '
                       '(websupported.age_id is not NULL OR age_id = 0) DESC,age_id ')
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["age_id"])

        # Choose a random row to test
        # random_line = randint(0, cursor.rowcount-1)

        # here we will check if the request is passed or failed
        count = resp.__len__()
        for x in range(count - 1):
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[x]["supported"], db_info[x][2]) is None) \
                    and (self.assertEqual(resp[x]["age_id"], db_info[x][0]) is None) \
                    and (self.assertEqual(resp[x]["name"], db_info[x][1]) is None):
                print ("Age Pass")
                print("resp: {0}".format(resp))
                print("Supported: {0}".format(resp[x]["supported"]))
                print("Age_Id: {0}".format(resp[x]["age_id"]))
                print("Name: {0}".format(resp[x]["name"]))



    def test_negative_2_age_missing_sendUnsupported(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("Login Pass")
                    print ("Token: {0}".format(resp["cookies"]))

                    # Parameters
                    user = {}
                    resp = Renderer_Functions.age(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Missing Parameter send_unsupported.") is None):
                                print ("-|-Result:Test case Pass")
                                print ("resp: {0}".format(resp))
                                print ("Status Code: {0}".format(resp["status_code"]))
                                print ("Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print ("-|-Result:Test case Failed")
                            print ("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print ("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Login Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))

    def test_negative_3_age_wrong_login(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.age("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_4_age_missing_login(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.age("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.age("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_5_age_incorrect_sendUnsupported(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("Login Pass")
                    print ("Token: {0}".format(resp["cookies"]))

                    # Parameters
                    user = {"send_unsupported" : "abc"}
                    resp = Renderer_Functions.age(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Incorrect value for send_unsupported.") is None):
                                print ("-|-Result:Test case Pass")
                                print ("resp: {0}".format(resp))
                                print ("Status Code: {0}".format(resp["status_code"]))
                                print ("Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print ("-|-Result:Test case Failed")
                            print ("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print ("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Login Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))

if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
