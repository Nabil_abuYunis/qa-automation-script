import unittest
import Renderer_Functions
from Database import Postgres
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBStopSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_stop(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        # Parameters
        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
        #             'is open on a holiday?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        # request_id = "654334"
        # #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # #file_format = "wav"

        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" ' + \
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that ' + \
        #             'is open on a holiday?</speak>'
        text_ssml = 'testing a new input by API'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        #Parameters
        filenumber = resp['results']['filenumber']

        user = {"filenumber": filenumber}
        resp = Renderer_Functions.send_request(cookies, user, "stop")

        if (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']['success_message'], "stopped")) is None:
             print("Stop Pass")
             print("resp: {0}".format(resp))
        else:
             print("Positive Test Failed")
             assert False

    print("Stop Pass")

    def test_negative_2_incorrect_filenumber(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        filenumber = "asdvkafsk"
        user = {"filenumber": filenumber}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "stop")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect value for filenumber.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_3_missing_filenumber(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "stop")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "missing parameter filenumber.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_4_stop_wrong_login(self):
        # Parameters
        user = {"filenumber": "68579"}
        resp = Renderer_Functions.send_request("123456789", user, "stop")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_5_stop_missing_login(self):
        # Parameters
        user = {"filenumber": "68579"}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "stop")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"filenumber": "68579"}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "stop")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)