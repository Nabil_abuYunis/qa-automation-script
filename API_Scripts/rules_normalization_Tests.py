import unittest
import Renderer_Functions
from Database import Postgres
import random
import string
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBRulesNormalizationSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)


    def test_positive_1_Rules_Normalization_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()

        # Parameters
        language_id = 6

        user = {"language_id": language_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "rulesnormalization")

        # Need permission question answered first

        # Fetch the info from the Database
        cursor = Postgres.connect_to_db_dictionary()
        query = "SELECT normalization_rules_id as id, rule_name as name FROM normalization_rules ORDER BY id"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["id"])

        # here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        count = resp.__len__()
        for x in range(count):
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[x]["id"], db_info[x][0]) is None) \
                    and (self.assertEqual(resp[x]["name"], db_info[x][1]) is None):
                print("Rules Normalization Pass")
                print("resp: {0}".format(resp))
                print("Id: {0}".format(resp[x]["id"]))
                print("Name: {0}".format(resp[x]["name"]))
            else:
                print("Positive Test Failed")
                assert False
        print("Rules Normalization Pass")

    def test_negative_2_Rules_Normalization_wrong_login(self):

        # Parameters
        language_id = 6

        user = {"language_id": language_id}
        resp = Renderer_Functions.send_request("123456789", user, "rulesnormalization")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_Rules_Normalization_missing_login(self):

        # Parameters
        language_id = 6

        user = {"language_id": language_id}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "rulesnormalization")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_Rules_Normalization_expired_token(self):

        # Parameters
        language_id = 6

        user = {"language_id": language_id}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "rulesnormalization")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
