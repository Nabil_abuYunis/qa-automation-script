import unittest
import Renderer_Functions as Renderer_Functions
import random
import string
from Database import Postgres
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBFeedbackSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Read config file
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print( "-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Completed running test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print( self.currentResult)

    def test_positive_1_feedback(self):
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        # user_id = "1099511627778"
        cursor = Postgres.connect_to_db()
        query = "select distinct speaker_voice_owner_id from smi_voice"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = [i[0] for i in db_info if i[0]!=0 and i[0] is not None]
        user_id = random.choice(db_info)

        # reply_email = "bogy@google.com"
        text = "testing_feedback_" + ''.join(random.choice(string.ascii_letters ) for ii in range(5))
        # user = {"voice_owner_id": user_id, "email": reply_email, "text": text}
        # user = {"voice_owner_id": user_id, "reply_to_email": reply_email, "message": text}
        user = {"voice_owner_id": user_id, "reply_email": email, "text": text}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "feedback")

        # here we will check if the voice detail is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["success_message"], "200") is None):
            print("Feedback Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_2_feeback_wrong_login(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        # user_id = "1099511627778"
        cursor = Postgres.connect_to_db()
        query = "select distinct speaker_voice_owner_id from smi_voice"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = [i[0] for i in db_info if i[0] != 0 and i[0] is not None]
        user_id = random.choice(db_info)

        # reply_email = "bogy@google.com"
        text = "testing_feedback_" + ''.join(random.choice(string.ascii_letters) for ii in range(5))
        # user = {"voice_owner_id": user_id, "email": reply_email, "text": text}
        # user = {"voice_owner_id": user_id, "reply_to_email": reply_email, "message": text}
        user = {"voice_owner_id": user_id, "reply_email": email, "text": text}
        resp = Renderer_Functions.send_request("123456789", user, "feedback")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_feedback_missing_login(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        # user_id = "1099511627778"
        cursor = Postgres.connect_to_db()
        query = "select distinct speaker_voice_owner_id from smi_voice"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = [i[0] for i in db_info if i[0] != 0 and i[0] is not None]
        user_id = random.choice(db_info)

        # reply_email = "bogy@google.com"
        text = "testing_feedback_" + ''.join(random.choice(string.ascii_letters) for ii in range(5))
        # user = {"voice_owner_id": user_id, "email": reply_email, "text": text}
        # user = {"voice_owner_id": user_id, "reply_to_email": reply_email, "message": text}
        user = {"voice_owner_id": user_id, "reply_email": email, "text": text}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "feedback")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_feedback_expired_token(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        # user_id = "1099511627778"
        cursor = Postgres.connect_to_db()
        query = "select distinct speaker_voice_owner_id from smi_voice"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = [i[0] for i in db_info if i[0] != 0 and i[0] is not None]
        user_id = random.choice(db_info)

        # reply_email = "bogy@google.com"
        text = "testing_feedback_" + ''.join(random.choice(string.ascii_letters) for ii in range(5))
        # user = {"voice_owner_id": user_id, "email": reply_email, "text": text}
        # user = {"voice_owner_id": user_id, "reply_to_email": reply_email, "message": text}
        user = {"voice_owner_id": user_id, "reply_email": email, "text": text}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "feedback")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print( "-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
