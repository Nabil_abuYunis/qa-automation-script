import unittest
import Renderer_Functions
from Database import Postgres
import random
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBProjectpermissionSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)


    def test_positive_1_projectpermission(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Get permission fro the logged in user, if it is 11, he see all the projects
        cursor = Postgres.connect_to_db()
        # query = "select user_permission_id from user_account_permissions where user_email ilike '" + email + "'"
        # cursor.execute(query)
        # user_permissions = [r for r in cursor]

        # Parameters
        # Get project id
        # Allowed projects
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)

        # Available projects with permissions
        query = "select distinct project_id from user_project_permissions"
        cursor.execute(query)
        projctIDS = [r for r in cursor]
        projctIDS_clean = [i[0] for i in projctIDS]

        # Intersect the two lists
        common_values = list(set(projctIDS_clean) & set(allowed_projects))
        random_project = random.choice(common_values)

        user = {"project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "projectpermission")

        # Fetch the info from the Database
        query = "select * from user_project_permissions where project_id ilike '" + random_project + "'"
        cursor.execute( query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"]["projects"], key=lambda k: k["user_email"])

        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        # here we will check if the request is passed or failed
        count = resp.__len__()
        for x in range(count):
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[x]["user_email"], db_info[x][0]) is None) \
                    and (self.assertEqual(resp[x]["project_id"], db_info[x][1]) is None) \
                    and (self.assertEqual(resp[x]["project_permission_id"], str(db_info[x][2])) is None) :
                print("user_email: {0}".format(resp[x]["user_email"]))
                print("project_id: {0}".format(resp[x]["project_id"]))
                print("project_permission_id: {0}".format(resp[x]["project_permission_id"]))
            else:
                print("Positive Test Failed")
                assert False

    print("Project permission Pass")

    def test_negative_2_projectpermission_wrong_login(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"send_unsupported": True, "project_id": random_project}
        resp = Renderer_Functions.send_request("123456789", user, "projectpermission")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_projectpermission_missing_login(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"send_unsupported": True, "project_id": random_project}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "projectpermission")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_projectpermission_expired_token(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"send_unsupported": True, "project_id": random_project}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "projectpermission")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
