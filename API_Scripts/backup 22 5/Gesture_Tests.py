import urllib.error as urllibe
import unittest
import Renderer_Functions
from Database import Postgres
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBGestureSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_gesture(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        cursor = Postgres.connect_to_db()
        # Choose domain that has gestures
        query = "select smi_domain_id from smi_domain_allowed_moods where mood_id in (select mood_id from mood where supported is TRUE AND mood_id<>10) " \
                + "INTERSECT " \
                + "select smi_domain_id from smi_domain_allowed_gestures where gesture_id in (select gesture_id from gesture where supported is TRUE) " \
                + "INTERSECT " \
                + "select DISTINCT smi_domain_id from smi_domain where language_id= 10" \
                + " ORDER BY smi_domain_id "
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_domain_id = random.choice(db_info)[0]

        # user = {"smi_domain_id": "72057594037927943"}
        user = {"smi_domain_id": smi_domain_id}

        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "gesture")
        count = resp['count']

        query = "WITH RECURSIVE parents(c_id, p_id, level) as (SELECT smi_domain_id, parent_id, 0 FROM " \
                "smi_domain  WHERE smi_domain_id = " + str(smi_domain_id) + " and marked_for_deletion is NULL  " \
                "UNION ALL SELECT smi_domain_id, parent_id, level + 1 FROM smi_domain INNER JOIN parents " \
                "ON (smi_domain_id = p_id)) SELECT smi_domain_id, domain_name, gesture_id, gesture_name," \
                " audio_url, img_url FROM smi_domain INNER JOIN(SELECT smi_domain_id, gesture_id, level, " \
                "rank() OVER(PARTITION BY gesture_id ORDER BY level), g.name gesture_name, audio_url, " \
                "img_url FROM smi_domain_allowed_gestures INNER JOIN parents ON(smi_domain_id=c_id) " \
                "INNER JOIN gesture g USING(gesture_id) WHERE supported   ) innerQuery USING(smi_domain_id)" \
                " WHERE rank = 1 ORDER BY 4"

        cursor.execute(query)
        db_info = [r for r in cursor]

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k['gesture_id'])

        # reorder dbinfo
        db_info = sorted(db_info, key=lambda k: k[2])

        # Img_Url_name_only = resp[random_line]["img_url"].split('/')
        # Audio_Url_name_only = resp[random_line]["audio_url"].split('/')

        # for x in gest_info:
        #     print(str(x[0]) + " - " + str(resp[random_line]["gesture_id"]))
        #     if str(x[0]) == str(resp[random_line]["gesture_id"]):
                # here we will check if the request is passed or failed

        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        count = resp.__len__()
        for x in range(count):
            if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp[x]['gesture_id'], db_info[x][2]) is None) \
                and (self.assertEqual(resp[x]["domain_name"], db_info[x][1]) is None) \
                and (self.assertEqual(resp[x]["smi_domain_id"], str(db_info[x][0])) is None) \
                and (self.assertEqual(resp[x]["img_url"].split('/')[4], db_info[x][5]) is None) \
                and (self.assertEqual(resp[x]["name"], db_info[x][3]) is None) \
                and (self.assertEqual(resp[x]["audio_url"].split('/')[4], db_info[x][4]) is None):
                    print("Gesture Pass")
                    print("resp: {0}".format(resp))
                    print("Gesture_Id: {0}".format(resp[x]['gesture_id']))
                    print("Domain: {0}".format(resp[x]["domain_name"]))
                    print("Domain Id: {0}".format(resp[x]["smi_domain_id"]))
                    print("Img_Url: {0}".format(resp[x]["img_url"]))
                    print("Name: {0}".format(resp[x]["name"]))
                    print("Audio_Url: {0}".format(resp[x]["audio_url"]))
            else:
                print("Positive Test Failed")
                assert False

        print("Gesture Pass")

    def test_negative_2_gesture_missing_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "gesture")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "Missing Parameter smi_domain_id.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_3_gesture_wrong_login(self):
        # Parameters
        user = {"smi_domain_id": "123456789"}
        resp = Renderer_Functions.send_request("123456789", user, "gesture")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_4_gesture_missing_login(self):
        # Parameters
        user = {"smi_domain_id": "123456789"}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "gesture")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_5_expired_token(self):
        # Parameters
        user = {"smi_domain_id": "123456789"}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "gesture")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_6_incorrect_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {"smi_domain_id":"it"}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "gesture")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "Incorrect value for smi_domain_id.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_7_not_found(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {"smi_domain_id":12345678912345678}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "gesture")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['count'], 0) is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['count']))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)