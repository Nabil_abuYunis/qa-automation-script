import unittest
import Renderer_Functions
from Database import Postgres
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBCancelSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)


    def test_positive_1_cancel(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]

        # text_ssml = Data_Utils.get_rand_ssml_value(config['LOCATION']['ssml'])
        # smi_voice_id = "72057594037928282"
        # domain_id = "72057594037927948"
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        # domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # request_id = "31523"
        # #request_id = "123456"
        # #file_format = "wav"

        text_ssml = 'testing a cancel input by API'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "31523"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])

        user = {"smi_voice_id": smi_voice_id, "request_id": str(request_id), "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}

        resp = Renderer_Functions.send_request(cookies, user, "say")

        # Parameters
        user = {"request_id" : request_id}
        resp = Renderer_Functions.send_request(cookies, user, "cancel")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["success_message"], "cancelled") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["success_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

        print("Account List Pass")

    def test_negative_2_missing_request_id_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]
        # text_ssml = Data_Utils.get_rand_ssml_value(config['LOCATION']['ssml'])
        # smi_voice_id = "72057594037928282"
        # domain_id = "72057594037927948"
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        # domain_id = Data_Utils.get_element_id(text_ssml, "domain")

        text_ssml = 'testing a cancel input by API'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "31523"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])

        # request_id = "31523"
        #request_id = "123456"
        #file_format = "wav"

        user = {"smi_voice_id": smi_voice_id, "request_id": str(request_id), "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.send_request(cookies, user, "say")

        # Parameters
        user = {}
        resp = Renderer_Functions.send_request(cookies, user, "cancel")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "Missing Parameter request_id.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_3_cancel_wrong_login(self):
        # Parameters
        user = {"request_id": "12345"}
        resp = Renderer_Functions.send_request("123456789", user, "cancel")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_4_cancel_missing_login(self):
        # Parameters
        user = {"request_id": "12345"}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "cancel")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_5_expired_token(self):
        # Parameters
        user = {"request_id" : "7164"}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "cancel")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    
if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
