import unittest
import Renderer_Functions
from Database import Postgres
import random
import configparser
import string

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBaddProjectSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Read config file
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)


    def test_positive_1_addProject(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']
        cookie = resp["cookies"][0]

        # Get account id
        # user = {}
        # resp = Renderer_Functions.send_request(cookie, user, "accountlist")
        # # Get account that have projects within from sql instead of API
        cursor = Postgres.connect_to_db()
        # email = self.config.get('LOGIN', 'email')
        # cursor.execute("SELECT distinct(account_id) FROM user_account_permissions where user_email like 'admin@speechmorphing.com' "
        #                "or user_email like '" + email + "'" + " in (select distinct account_id from project) "
        #                 "intersect (select distinct account_id from project)")
        # db_info = [r for r in cursor]
        # # random_line = randint(0, resp["count"] - 1)
        # # account_id = resp["results"][random_line]["account_id"]
        # random_account = random.choice(db_info)
        # random_account = 'C4398046511107'

        # Parameters
        allowed_accounts = Renderer_Functions.get_allowed_accounts(email)
        random_account = random.choice(allowed_accounts)

        project_name = ''.join(random.choice(string.ascii_letters) for ii in range(5))
        project_name = 'testPR_API' + project_name
        user = {"account_id": random_account[0], "project_name": project_name}
        resp = Renderer_Functions.send_request(cookie, user, "addProject")

        # Fetch projects from the Database for the same random account id
        query = "select * from project where account_id ilike '" + random_account[
            0] + "' and project_name ilike '" + project_name + "'"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"][0]["project_name"], db_info[0][2]) is None) \
                and (self.assertEqual(resp["results"][0]["project_id"], db_info[0][0]) is None) :
            print ("Add project Pass")
            print("resp: {0}".format(resp))
            print("project name: {0}".format(resp["results"][0]["project_name"]))
            print("project id: {0}".format(resp["results"][0]["project_id"]))

            f = open("Added_Parameters.txt", "a+")
            f.write("\n")
            f.write("project id: {0}" + resp["results"][0]["project_id"])
            f.write("\n")
            f.write("project name: {0} " + resp["results"][0]["project_name"])
            f.write("\n")
        else :
            print("Positive Test Failed")
            assert False
        print("Add project Pass")

    def test_negative_2_addProject_wrong_login(self):
        # Parameters
        email = self.config["LOGINADMIN"]["email"]
        allowed_accounts = Renderer_Functions.get_allowed_accounts(email)
        random_account = random.choice(allowed_accounts)

        project_name = ''.join(random.choice(string.ascii_letters) for ii in range(5))
        project_name = 'testPR_' + project_name

        user = {"account_id": random_account[0], "project_name": project_name}
        resp = Renderer_Functions.send_request("123456789", user, "addProject")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_addProject_missing_login(self):
        # Parameters
        email = self.config["LOGIN"]["email"]
        allowed_accounts = Renderer_Functions.get_allowed_accounts(email)
        random_account = random.choice(allowed_accounts)

        project_name = ''.join(random.choice(string.ascii_letters) for ii in range(5))
        project_name = 'testPR_' + project_name

        user = {"send_unsupported": True, "account_id": random_account[0], "project_name": project_name}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "addProject")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_addProject_expired_token(self):
        # Parameters
        email = self.config["LOGIN"]["email"]
        allowed_accounts = Renderer_Functions.get_allowed_accounts(email)
        random_account = random.choice(allowed_accounts)

        project_name = ''.join(random.choice(string.ascii_letters) for ii in range(5))
        project_name = 'testPR_' + project_name

        user = {"send_unsupported": True, "account_id": random_account[0], "project_name": project_name}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "addProject")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
