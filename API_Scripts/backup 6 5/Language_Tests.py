import urllib.error as urllibe
import unittest
import Renderer_Functions
from Database import Postgres

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBLanguageSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult))
        print(self.currentResult)

    def test_positive_1_Language_positive(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "language")

        # Fetch the info from the Database
        cursor = Postgres.connect_to_db()

        query = """WITH webSupported as (SELECT DISTINCT language_id FROM smi_voice where smi_voice_id
                        IN (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)) 
                       select language_id, name, supported, (websupported.language_id is not NULL OR 
                       language_id = 0) web_supported from language LEFT OUTER JOIN websupported using 
                       (language_id)ORDER BY (websupported.language_id is not NULL OR language_id = 0) 
                       DESC,language_id"""
        cursor.execute(query)
        curs = cursor.fetchall()
        db_info = [r for r in curs]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k['language_id'])

        # here we will check if the request is passed or failed
        count = resp.__len__()
        for x in range(count):
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[x]["supported"], db_info[x][2]) is None) \
                    and (self.assertEqual(resp[x]['language_id'], db_info[x][0]) is None) \
                    and (self.assertEqual(resp[x]["name"], db_info[x][1]) is None):
                    # and (self.assertEqual(resp[x]['is_rtl'], db_info[x][3]) is None)
                print("Language Pass")
                print("resp: {0}".format(resp))
                print("Supported: {0}".format(resp[x]["supported"]))
                print("Language_Id: {0}".format(resp[x]['language_id']))
                print("resp: {0}".format(resp[x]["name"]))

    def test_negative_2_Language_missing_sendUnsupported(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # no parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "language")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter send_unsupported.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_3_Language_wrong_login(self):

        # no parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("123456789", user, "language")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_4_Language_missing_login(self):
        # no parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "language")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_5_Language_incorrect_send_unsupported(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # no parameters
        user = {"send_unsupported" : "shephard"}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "language")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for send_unsupported.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "language")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)