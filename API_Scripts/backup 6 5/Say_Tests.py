import urllib.error as urllibe
import unittest
import Renderer_Functions
from Database import Postgres
import JSON_Request as Data_Utils
import WAV_Logs as wav
import random
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBSaySuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_say(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
        #             'is open on a holiday?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        # request_id = "654334"
        # #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # #file_format = "wav"

        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" ' + \
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that ' + \
        #             'is open on a holiday?</speak>'
        text_ssml = 'testing a new input by API'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        print("***********VALIDATING API RESPONSE**********")
        if resp == None:
            print("-----No response back-----")

        print("Response Status Code: {0}".format(resp["status_code"]))
        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and self.assertGreater(resp["results"]["filenumber"], 0) is None \
                and self.assertIn(".wav", resp["results"]["filename"]) is None :
            # if wav.check_wav_files(resp,"",True):
                print ("Get Voice Pass")
                print ("resp: {0}".format(resp))
                print ("status_code: {0}".format(resp["status_code"]))
                print ("Filename: {0}".format(resp['results']['filename']))
        else:
            print("FAILURE: response code was not 200")

    def test_positive_2_say_optional_file_format(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
        #             'is open on a holiday?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        # request_id = "654334"
        # #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # #file_format = "wav"

        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" ' + \
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that ' + \
        #             'is open on a holiday?</speak>'
        text_ssml = 'testing a new input by API'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true")
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        # Choose random file format value
        all_file_formats = ["wav", "pcma", "pcmu"]
        file_format = random.choice(all_file_formats)
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "file_format": file_format, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        print("***********VALIDATING API RESPONSE**********")
        if resp == None:
            print("-----No response back-----")

        print("Response Status Code: {0}".format(resp["status_code"]))
        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and self.assertGreater(resp["results"]["filenumber"], 0) is None \
                and self.assertIn("." + file_format, resp["results"]["filename"]) is None :
            # if wav.check_wav_files(resp,"",True):
                print ("Get Voice Pass")
                print ("resp: {0}".format(resp))
                print ("status_code: {0}".format(resp["status_code"]))
                print ("Filename: {0}".format(resp['results']['filename']))
        else:
            print("FAILURE: response code was not 200")

    def test_positive_3_say_optional_url_type(self):
        # mibo
        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
        #             'is open on a holiday?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        # request_id = "654334"
        # #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # #file_format = "wav"

        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" ' + \
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that ' + \
        #             'is open on a holiday?</speak>'
        text_ssml = 'testing a new input by API'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true")
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        all_url_types = ["http", "smb"]
        url_type = random.choice(all_url_types)
        # url_type = "smb"

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "url_type": url_type, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        # print("***********VALIDATING API RESPONSE**********")
        # if resp == None:
        #     print("-----No response back-----")

        # print("Response Status Code: {0}".format(resp["status_code"]))
        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and self.assertGreater(resp["results"]["filenumber"], 0) is None \
                and self.assertIn(url_type, resp["results"]["filename"]) is None:
            # if wav.check_wav_files(resp,"",True):
            print("Get Voice Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))
            print("Filename: {0}".format(resp['results']['filename']))
        else:
            print("FAILURE: response code was not 200")

    def test_negative_4_say_wrong_smi_voice_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        smi_voice_id = "ABCD"
        request_id = "215020"
        smi_domain_id = "1"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["results"]["error_message"],
                                              "Incorrect value for smi_voice_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["results"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_5_say_missing_smi_voice_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        request_id = "215020"
        smi_domain_id = "1"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["results"]["error_message"],
                                              "Missing Parameter smi_voice_id") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["results"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_6_say_wrong_request_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        smi_voice_id = "72057594037928290"
        request_id = 123456
        smi_domain_id = "72057594037927948"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["results"]["error_message"],
                                              "Incorrect value for request_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["results"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_7_say_missing_request_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        smi_voice_id = "72057594037927940"
        smi_domain_id = "72057594037927948"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["results"]["error_message"],
                                              "Missing Parameter request_id") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["results"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_8_say_wrong_domain_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        smi_voice_id = "72057594037927940"
        request_id = "215020"
        smi_domain_id = "A"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["results"]["error_message"],
                                              "Incorrect value for smi_domain_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["results"]["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_9_say_missing_domain_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        smi_voice_id = "72057594037927940"
        request_id = "215020"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["results"]["error_message"],
                                              "Missing Parameter smi_domain_id") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["results"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_10_say_wrong_text_sml(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        smi_voice_id = "72057594037928290"
        request_id = "215020"
        smi_domain_id = "72057594037927948"
        text_ssml = 123456789
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["results"]["error_message"],
                                              "Incorrect value for text_ssml.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["results"]["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_11_say_missing_text_ssml(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        smi_voice_id = "72057594037927940"
        request_id = "215020"
        smi_domain_id = "1"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["results"]["error_message"],
                                              "Missing Parameter text_ssml.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["results"]["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_12_say_wrong_login(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        smi_voice_id = "72057594037927940"
        request_id = "215020"
        smi_domain_id = "1"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request("123456789", user, "say")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))


            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_13_say_missing_login(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        smi_voice_id = "72057594037927940"
        request_id = "215020"
        smi_domain_id = "1"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "say")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_14_say_no_permission_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        cursor = Postgres.connect_to_db()
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where supported = false")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        # smi_voice_id = "72057594037928290"
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "215020"
        # smi_domain_id = "72057594037927940"
        text_ssml = ""
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["results"]["error_message"],
                                              " you do not have permission to use this domain. ") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["results"]))
                    print("Reason: {0}".format(resp["results"]["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_15_say_no_permission_voice(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        cursor = Postgres.connect_to_db()
        cursor.execute(
            "select Distinct smi_voice_id from smi_voice where supported = false")

        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "215020"
        smi_domain_id = "72057594037927948"
        text_ssml = "special ssml"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["results"]["error_message"],
                                              " you do not have permission to use this voice. ") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["results"]["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_16_say_no_permission_style_gest_mood(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        cursor = Postgres.connect_to_db()
        # Select smi_domain_id and their gestures gestures
        query = "select smi_domain_id, gesture_id from smi_domain_allowed_gestures where gesture_id in (select gesture_id from gesture where supported = False) " \
                + "AND smi_domain_id in (select smi_domain_id from smi_domain where supported AND open_to_public) ORDER BY smi_domain_id "
        cursor.execute(query)
        db_info = [r for r in cursor]
        # Get random smi_domain_id and it's gesture
        random_line = random.choice(db_info)

        smi_domain_id = random_line[0]
        gesture_id = random_line[1]

        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        # db_info = [r for r in cursor]
        # smi_voice_id = int(random.choice(db_info)[0])

        # Select unsupported gestures
        query = "select gesture_id from gesture where supported = false"
        cursor.execute(query)
        db_info = [r for r in cursor]
        gesture_id = random.choice(db_info)
        # gesture_id = 68

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak version="1.0" xml:lang="0" xml:domain="' + str(smi_domain_id) + '" xml:accent="default" xml:rate="100" xml:volume="100" xml:pitch="100" xml:style="72057594037927941" xml:mood="undefined" xml:voice="72057594037928290"><gesture value="' + str(gesture_id) + '"></gesture>wild and free</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        # db_info = [r for r in cursor]
        # smi_voice_id = int(random.choice(db_info)[0])
        permitted_voices = Renderer_Functions.get_permitted_voices_for_account_from_DB(email, language_id)
        smi_voice_id = random.choice(permitted_voices)
        request_id = "abc"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["results"]["error_message"],
                                              "Incorrect value for smi_voice_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["results"]["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_17_say_wrong_file_format(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        smi_voice_id = "72057594037928290"
        request_id = "654334"
        smi_domain_id = "72057594037927948"
        file_format = "1234"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "file_format": file_format, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["results"]["error_message"],
                                              " the accepted values for format are wav, pcma, pcmu, raw.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["results"]["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_18_expired_token(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        smi_voice_id = "72057594037927940"
        request_id = "215020"
        smi_domain_id = "1"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "say")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_positive_19_wrong_language(self):
        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="' + str(language_id) + '" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a water fountain?</speak>'
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true")
        # db_info = [r for r in cursor]
        # smi_voice_id = int(random.choice(db_info)[0])
        allowed_voices_in_allowed_accounts = Renderer_Functions.get_permitted_voices_for_account_from_DB(email, "6")
        smi_voice_id = random.choice(allowed_voices_in_allowed_accounts)
        request_id = "654334"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        file_format = "wav"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "file_format": file_format, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "say")

        print("***********VALIDATING API RESPONSE**********")
        if resp == None:
            print("-----No response back-----")

        print("Response Status Code: {0}".format(resp["status_code"]))
        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None:
            if wav.check_wav_files(resp,"",True):
                print ("Get Voice Pass")
                print ("resp: {0}".format(resp))
                print ("status_code: {0}".format(resp["status_code"]))
                print ("Filename: {0}".format(resp['results']['filename']))
        else:
            print("FAILURE: response code was not 200")

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)