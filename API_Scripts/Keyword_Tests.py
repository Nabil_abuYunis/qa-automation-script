import unittest
import Renderer_Functions
from Database import Postgres
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBKeywordSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)

    def test_positive_1_keyword(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        # Get smi_domain_id that have keywords
        cursor = Postgres.connect_to_db()
        cursor.execute("select distinct smi_domain_id from smi_domain_keywords")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])

        # get keywords from the API
        user = {"smi_domain_id": smi_domain_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "keyword")

        # get the keywords for the chosen domain and its parents
        has_parent = True
        all_keywords = []
        while has_parent:
            cursor.execute(
                'select keyword from smi_domain_keywords where smi_domain_id = ' + str(smi_domain_id))
            db_info = [r for r in cursor]
            if db_info.__len__() != 0:
                for var in db_info:
                    all_keywords.append(var[0])

            # Check if a domain has a parent
            cursor.execute("select parent_id from smi_domain WHERE smi_domain_id = " + str(smi_domain_id))
            db_info = [r for r in cursor]
            if db_info[0][0] is not None:
                smi_domain_id = db_info[0][0]
            else:
                has_parent = False

        # all_keywords = sorted(all_keywords, key=lambda k: k[0])
        all_keywords.sort()
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k['keyword'])
        # Choose a random row to test
        # random_line = randint(0, resp['count']-1)

        # here we will check if the request is passed or failed
        count = resp.__len__()

        # If they are not the same amount then fail
        if resp.__len__() != all_keywords.__len__():
            assert False

        for x in range(count):
            print("DB Keyword: {0}".format(all_keywords[x]))
            # print ("SMI_Domain_Id: {0}".format(resp['results'][x]["smi_domain_id"]))
            print("resp keyword: {0}".format(resp[x]["keyword"]))
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[x]["keyword"], all_keywords[x])) is None:
                    # and (self.assertEqual(resp['results'][x]["smi_domain_id"], db_info[x][0]) is None) \
                print ("Keyword Pass")
                print ("resp: {0}".format(resp))
            else:
                print("Positive Test Failed")
                assert False
    print("Keyword Pass")

    def test_negative_2_missing_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "keyword")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "missing parameter smi_domain_id.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_3_incorrect_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {"smi_domain_id": "hello"}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "keyword")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "incorrect value for smi_domain_id.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_4_wrong_login(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("123456789", user, "keyword")

        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_5_domain_missing_login(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "keyword")

        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "keyword")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)