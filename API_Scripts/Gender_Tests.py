import unittest
import Renderer_Functions
from Database import Postgres

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBGenderSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print( "-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Completed running test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print( self.currentResult)

    def test_positive_1_gender(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "gender")

        # Fetch the info from Database
        cursor = Postgres.connect_to_db()
        cursor.execute("WITH webSupported as (SELECT DISTINCT gender_id FROM smi_voice where smi_voice_id IN (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)) select gender_id, name, supported, (websupported.gender_id is not NULL OR gender_id = 0) web_supported from gender LEFT OUTER JOIN websupported using (gender_id) ORDER BY (websupported.gender_id is not NULL OR gender_id = 0) DESC, gender.gender_id ")

        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k['gender_id'])

        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        count = resp.__len__()
        for x in range(count):
            # here we will check if the request is passed or failed
            #and (self.assertEqual(resp[random_line]['img_url'], db_info[random_line][3]) is None) \
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[x]['gender_id'], db_info[x][0]) is None) \
                    and (self.assertEqual(resp[x]["supported"], db_info[x][2]) is None) \
                    and (self.assertEqual(resp[x]["name"], db_info[x][1]) is None):
                print( "Gender Pass")
                print( "resp: {0}".format(resp))
                print( "Gender_Id: {0}".format(resp[x]['gender_id']))
                print( "Supported: {0}".format(resp[x]["supported"]))
                print( "Name: {0}".format(resp[x]["name"]))
                #print( "IMG_Url: {0}".format(resp[random_line]["img_url"]))
            else:
                print("Positive Test Failed")
                assert False

        print("Gender Pass")

    def test_negative_2_gender_missing_sendUnsupported(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "gender")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "missing parameter send_unsupported.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_3_gender_wrong_login(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("123456789", user, "gender")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_4_gender_missing_login(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "gender")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_5_gender_incorrect_sendUnsupported(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {"send_unsupported" : "news"}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "gender")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "incorrect value for send_unsupported.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "gender")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

if __name__ == '__main__':
    print( "-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)