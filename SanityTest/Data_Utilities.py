import random
from random import randrange
import os

def get_ssml_value(path):
    ssml_file = random.choice(os.listdir(path))
    with open(path + "\\" + ssml_file, encoding="utf8") as ssml:
        text = ssml.read()
        domain = text.split('\n')
        stuff = ""
        for a in domain:
            stuff += a
        return stuff

def get_element_id(text_ssml, element):
    arr = text_ssml.split(' ')
    for a in arr:
        if element in a:
            b = a.split("\"")
            return(b[1])

def get_line_of_text(path):
    flag = True

    while flag:
        with open(path, encoding="utf8") as f:
            line = next(f)
            for num, aline in enumerate(f):
                if randrange(num + 2): continue
                line = aline

            if not line in ['\n', '\r\n'] and line != "":
                flag = False
                return line     

def incrId(num):
    num += 1
    return str(num)

def clean_word(word):
    word = re.sub("&", "&amp;", word)
    word = re.sub("<", "&lt;", word)
    word = re.sub(">", "&gt;", word)
    word = re.sub("\"", "&quot;", word)
    word = re.sub("'", "&apos;", word)
    print("Galactic: " + word)
    return word.strip('\n')
