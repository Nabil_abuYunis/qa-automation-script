# README #

SMI sanity testing script (Python 3)

###Relevant scripts###
 - Renderer_Functions.py
 - sanityrunner.py

 Note: Renderer_Functions.py was adapted to Python 3 from API Automation Tests.
       Plan should be to add onto the class for API testing
 
### Description ###
Script is designed to run requests continously through Smorph API.
Reads from a text file and then constructs POST requests to "say" endpoint
Can send API requests synchronously and asynchronously.
Generates log file with results.

### Run Tests###
Run "sanityrunner.py" with synchronous requests using command "python sanityrunner.py SMISanityTest.test_synch"
Run "sanityrunner.py" with asynchronous requests using command "python sanityrunner.py SMISanityTest.test_asynch"

### More info about the tests ###
- Async and synch requests read urls differently. Urls defined in runner method for async and in Renderer_Functions.py for sync
- Pulls random lines from text file
- logs stored in "logs" folder

### Future dev ###
- Improved error handling
- Randomize domain_id, voice_id, and request_id
